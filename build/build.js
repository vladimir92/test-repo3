// https://github.com/shelljs/shelljs
require('shelljs/global')

if (!process.env.FOLDER) {
  process.env.FOLDER = 'taponit';
}

var path = require('path')
var config = require('../config')
var ora = require('ora')
var webpack = require('webpack')
var webpackConfig = require('./webpack.prod.conf')

console.log(
  '  Tip:\n' +
  '  Built files are meant to be served over an HTTP server.\n' +
  '  Opening index.html over file:// won\'t work.\n'
)

var spinner = ora('building for production...')
spinner.start()

var assetsPath = path.join(config.build.assetsRoot, config.build.assetsSubDirectory)
rm('-rf', assetsPath)
mkdir('-p', assetsPath)
cp('-R', 'static/', assetsPath)
cp( '-R', path.resolve(__dirname, `../src/assets/themes/${process.env.FOLDER}/images/icons`), path.resolve(assetsPath, './images'));
cp( '-R', path.resolve(__dirname, `../src/assets/themes/${process.env.FOLDER}/images/icons/favicon.ico`), assetsPath);

webpack(webpackConfig, function (err, stats) {
  spinner.stop()
  if (err) throw err
  process.stdout.write(stats.toString({
    colors: true,
    modules: false,
    children: false,
    chunks: false,
    chunkModules: false
  }) + '\n')
})
