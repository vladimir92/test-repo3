FROM node:10.15.1

RUN apt-get update && apt-get install -y \
 libgtk2.0-0 libgconf-2-4 libasound2 libxtst6 libxss1 libnss3 xvfb fonts-liberation libappindicator3-1 \
 libatk-bridge2.0-0 libatspi2.0-0 libgtk-3-0 lsb-release xdg-utils \
  && wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
  && dpkg -i google-chrome-stable_current_amd64.deb

WORKDIR /usr/src/app

COPY . .

RUN npm install

CMD [ "npm", "run", "docker:e2e" ]
