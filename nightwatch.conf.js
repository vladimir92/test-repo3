module.exports = ((settings) => {
  settings.launch_url = 'http://localhost:' + process.env.PORT;
  return settings;
})(require('./nightwatch.json'));
