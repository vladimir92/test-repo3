const url = '?subscriberData=%7B%22subscriberGUID%22%3A%2287B59006-D2A7-42B0-B516-7A6E5CF8B85F%22%7D';
const data2 = {
  description: ''
};

const navigateToOffer = (browser, selectIndex) =>
  browser
    .url(browser.launch_url + url)
    .waitForElementVisible('body', 'Body loaded')
    .isVisible('.overlay2-container > .overlay2 > button', ({ value }) => {
      if (value) {
        browser
          .click({
            selector: '.overlay2-container > .overlay2 > button',
            index: 1
          });
      }
    })
    .pause(2000)
    .waitForElementVisible('.bottomBox > a', 'Offer visible on landing page')
    .getText({
      index: selectIndex,
      selector: '.business-name'
    }, (data) => {
      data2.description = data.value;
    })
    .click({
      selector: '.bottomBox > a',
      index: selectIndex
    })
    .waitForElementVisible('.offer-container2 > h1', 7000, 'Offer business name visible')
    .assert.containsText('.offer-container2 > h1', data2.description, 'Offer business name match');

module.exports = {
  beforeEach() {
    data2.description = '';
  },
  'Opening already claimed offer'() {
    const selectIndex = 1;
    navigateToOffer(this.client, selectIndex)
      .getLocationInView('.redeem-button.button.vhcenter')
      .click('.redeem-button')
      .waitForElementPresent('.overlay.overlay-redeem.overlay-redeem2 .stop > h1',
        'Redeemed overlay present')
      .assert.containsText('.overlay.overlay-redeem.overlay-redeem2 .stop > h1',
        'You\'ve successfully redeemed this offer!',
        'Redeemed overlay contains proper text')
      .click('.overlay.overlay-redeem.overlay-redeem2 .stop > h1')
      .end();
  },
  'Opening not claimed offer'() {
    const selectIndex = 2;

    navigateToOffer(this.client, selectIndex)
      .getLocationInView('.redeem-button.button.vhcenter')
      .click('.redeem-button')
      .waitForElementPresent('.button.button2.redeem-button', 'Redeem button became present')
      .assert.containsText('.button.button2.redeem-button',
        'Redeem Now',
        'Button contain expected text')
      // .click('.button.button2.redeem-button')
      // .waitForElementNotPresent('.button.button2.redeem-button', 'Redeem overlay disappeared')
      // .getLocationInView('.redeem-button.button.vhcenter')
      // .click('.redeem-button')
      // .waitForElementPresent('.overlay.overlay-redeem.overlay-redeem2 .stop > h1', 'Redeemed overlay present')
      // .assert.containsText('.overlay.overlay-redeem.overlay-redeem2 .stop > h1',
      //   'You\'ve successfully redeemed this offer!',
      //   'Redeemed overlay contains proper text')
      // .click('.overlay.overlay-redeem.overlay-redeem2 .stop > h1')
      .end();
  }
};
