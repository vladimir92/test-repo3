export default {
  data() {
    return {
      errors: {
        present: false,
        name: '',
        fields: []
      },
      loading: false,
      saved: false
    }
  },
  methods: {
    submitForm: function(options, e) {
      this.loading = true;
      e.preventDefault()
      this.$validate()

      if (this.$validation.valid) {
        var httpAction = options.httpAction

        this.$http[httpAction](this.$config('baseURL') + '/' + options.resource + '/save', this.form).then(
          (response) => {
            if (response.json().metadata) {
              this.errors.present = false
              this.errors.name = ''
              this.errors.fields = []
              this.saved = true
            }
            if (response.json().error) {
              this.errors.present = true
              this.errors.name = response.json().error
              this.errors.fields = response.json().errorOn
              this.setErrors()
              this.saved = false
            }
            this.loading = false;
          }, (response) => {
            this.loading = false;
          })
      } else {
        this.loading = false;
      }
    },
    setErrors: function() {
      this.errors.fields.forEach(function(element) {
        if (this.form[element]) {
          this.$setValidationErrors([{ field: element, message: 'Error' }])
        }
      }, this)
    }
  }
}
