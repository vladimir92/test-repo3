// This is needed for the prerender server to understand the vue-analytics module
require('es6-promise').polyfill();
require('./pollyfills.js');

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VueFormular from 'vue-formular'
import VueTouch from 'vue-touch'
import VueHead from 'vue-head'
import VueMoment from 'vue-moment'
import VueValidator from 'vue-validator'
import VueAnalytics from 'vue-analytics';
import Cookie from 'js-cookie';

import appConfig from '../config'

Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(VueFormular)
Vue.use(VueTouch)
Vue.use(VueHead)
Vue.use(VueMoment)
Vue.use(VueValidator)

var router = new VueRouter({hashbang: false, history: true})

import App from './App'

Vue.prototype.$appConfig = function(key) {
  if (appConfig) {
    return key === '*' ? appConfig : appConfig[key]
  }

  return ''
}

const preventionAPI = {
  url: 'http://preventThis',
  body: { records: [] }
};

Vue.prototype.$config = function(key) {
  if (!this.$root && key === 'baseURL') {
    return preventionAPI.url;
  }

  var config = this.$root.$get('config')

  if (config) { return config[key] }

  return ''
}

document.title = appConfig.build.env.siteName;

Vue.prototype.copyright = function() {
  return this.$root.$get('config')['copyright'] ||
    `Copyright © 2018, ${this.$root.$get('config')['siteName']}`;
}

Vue.component('version', require('./components/partials/version.vue'));
Vue.component('facebook', require('./components/partials/facebook.vue'));
// socialbar is never used.
Vue.component('socialbar', require('./components/partials/socialbar.vue'));
Vue.component('menu', require('./components/partials/menu.vue'));
Vue.component('joi-header', require('./components/partials/joi-header.vue'));

const campaign = Vue.component('campaign', require('./components/campaign/index.vue'));
const contest = Vue.component('contest', require('./components/contest/index.vue'));
const home = Vue.component('home', require('./components/home/index.vue'));
const moreOffers = Vue.component('more-offers', require('./components/more-offers/index.vue'));
const offer = Vue.component('offer', require('./components/offer/index.vue'));
const profile = Vue.component('profile', require('./components/profile/index.vue'));
const redirect = Vue.component('redirect', require('./components/redirect/index.vue'));
const saved = Vue.component('saved', require('./components/saved/index.vue'));
const selectMarkets = Vue.component('select-market', require('./components/select-markets/index.vue'));
const sharedOffer = Vue.component('shared-offer', require('./components/shared-offer/index.vue'));
const singleOffer = Vue.component('single-offer', require('./components/single-offer/index.vue'));
const preview = Vue.component('preview', require('./components/preview/index.vue'));
const tutorial = Vue.component('tutorial', require('./components/tutorial/tutorial.vue'));

// TODO: resolve jquery global
Vue.transition('fadeStatusOut', {
  afterEnter: function(el) {
    const self = this;
    const jQuery = window.jQuery || window.$;
    window.setTimeout(() => { jQuery(el).fadeOut(); self.saved = false;}, 5000);
  }
});

Vue.transition('fadeModal', {
  beforeEnter: function(el) {
    const jQuery = window.jQuery || window.$;
    jQuery(el).fadeIn(300);
  },
  leave: function(el, done) {
    const jQuery = window.jQuery || window.$;
    jQuery(el).fadeOut(300, () => {
      done();
    });
  }
});

router.map({
  '/': {
    component: home
  },
  '/c/:urlToken': {
    component: contest
  },
  '/contest/:marketID': {
    component: contest
  },
  '/ca': {
    component: campaign
  },
  '/ca/preview/:urlCode': {
    component: campaign
  },
  '/ca/:campaignID': {
    component: campaign
  },
  '/ca/:campaignID/:subscriberGUID': {
    component: campaign
  },
  '/ca/:campaignID/:subscriberGUID/:marketID': {
    component: campaign
  },
  '/l/:shortURL': {
    component: redirect
  },
  '/l/preview/:previewCode': {
    component: redirect
  },
  '/o': {
    component: offer
  },
  '/o/:offerGUID': {
    component: offer
  },
  '/o/:offerGUID/:subscriberGUID': {
    component: offer
  },
  '/o/:offerGUID/:subscriberGUID/:campaignID': {
    component: offer
  },
  '/more-offers': {
    component: moreOffers
  },
  '/more-offers/:subscriberGUID': {
    component: moreOffers
  },
  '/offer/:offerGUID': {
    component: singleOffer
  },
  '/offer/:offerGUID/:subscriberGUID': {
    component: singleOffer
  },
  '/saved': {
    component: saved
  },
  '/so': {
    component: sharedOffer
  },
  '/so/*any': {
    component: sharedOffer
  },
  '/so/:offerGUID/:subscriberGUID': {
    component: sharedOffer
  },
  '/sm/:subscriberGUID': {
    component: selectMarkets
  },
  '/preview': {
    component: preview
  },
  '/profile': {
    component: profile
  },
  '/tutorial': {
    component: tutorial
  }
});

router.redirect({
  '/c/pc916': '/contest/3',
  '/c/crc1016': '/contest/4',
  '/c/dm117': '/contest/9'
});

Vue.use(VueAnalytics, {
  id: 'UA-124920783-2',
  router
});

const sendTrackImpression = (toComponent, fromComponent, customData, location = {}) => {
  if (!Vue.prototype.validMarket) {
    return;
  }

  if (fromComponent === 'Redirect') {
    fromComponent = Cookie.get('targetURL') || fromComponent;
    Cookie.remove('targetURL');
  }

  const referrerType = /https?:\/\//.test(fromComponent) ? 'url' : 'page';
  if (referrerType === 'url') {
    fromComponent = [
      '',
      ...fromComponent.split(/\/|#!/).filter(e => e).slice(2)
    ].join('/');
  }

  const data = {
    referral: fromComponent,
    page: toComponent,
    subscriberGUID: Cookie.get('subscriberGUID'),
    localTimezone: new Date().getTimezoneOffset() / 60,
    customData,
    referrerType,
    latitude: undefined,
    longitude: undefined,
    userAgent: navigator.userAgent
  };

  if (location.coords) {
    data.latitude = location.coords.latitude;
    data.longitude = location.coords.longitude;
  }

  Vue.http.post(url + '/subscribers/impression', data)
    .catch(console.warn);
};

router.beforeEach((transition) => {
  const toComponent = transition.to.matched ? transition.to.matched[0].handler.component.name : document.referrer;
  const fromComponent = transition.from.matched ? transition.from.matched[0].handler.component.name : document.referrer;
  if (['SingleOffer', 'Home', 'MoreOffers'].includes(toComponent)) {
    const customData = toComponent === 'SingleOffer'
      ? { offerGUID: transition.to.params.offerGUID }
      : {
        campaignID: Cookie.get('lastCampaignID'),
        marketID: Cookie.get('lastMarketID')
      };

    if (navigator.permissions) {
      navigator.permissions.query({ name: 'geolocation' })
        .then(permission => {
          if (permission.state !== 'prompt') {
            navigator.geolocation.getCurrentPosition(
              (location) => sendTrackImpression(toComponent, fromComponent, customData, location),
              () => sendTrackImpression(toComponent, fromComponent, customData));
          } else {
            sendTrackImpression(toComponent, fromComponent, customData);
          }
        });
    }
  }

  return transition.next();
});

Vue.http.options.credentials = true;

const url = appConfig.build.env.baseURL.replace(/"/g, '');

const itemsInBoth = ['subscriberGUID', 'lastCampaignID', 'lastMarketID'];

const checkCookieAndLocalStorage = () => {
  itemsInBoth.forEach((e) => {
    const value = Cookie.get(e);
    const local = window.localStorage.getItem(e);
    if (!value) {
      Cookie.set(e, local);
    } else if (value !== local) {
      window.localStorage.setItem(e, value);
    }
  });
};

const calculateMarket = (marketID = -1, skipFirstCheck = false) => {
  checkCookieAndLocalStorage();
  if (!skipFirstCheck && /so$|ca$|preview$/.test(window.location.pathname.split(/\/\/|\//)[1])) {
    Vue.prototype.validMarket = false;
    return Promise.resolve(false);
  }

  const newDesign = true;
  Vue.prototype.validMarket = newDesign;
  return Promise.resolve(newDesign);
};

const evaluateMarkets = (...args) => calculateMarket(...args);

Vue.prototype.evaluateMarkets = evaluateMarkets;
Vue.prototype.checkCookieAndLocalStorage = checkCookieAndLocalStorage;

if (window.location === window.parent.location) {
  evaluateMarkets()
    .then(() => Vue.http.get(url + '/settings/getAll'))
    .then(response => {
      const settingsData = JSON.parse(response.body).records;
      const settings = {};
      settingsData.forEach(setting => {
        settings[setting.name] = setting.value;
      });
      Vue.prototype.$settings = settings;
      router.start(App, '#app')
    }).catch(e => {
      console.error(e);
      router.start(App, '#app')
    });
} else {
  router.start(App, '#app');
}

Vue.http.interceptors.push((request, next) => {
  if (request.url.includes(preventionAPI.url)) {
    return request.respondWith(preventionAPI.url);
  }
  return next();
});
