import Cookie from 'js-cookie'
import _ from 'lodash'
import toiArrayHelper from '../../assets/scripts/array-helpers.js'
import moment from 'moment'

export default {
  data () {
    const shortCode = this.$appConfig('build').env.shortCode || '82928';
    const optinKeyword = this.$appConfig('build').env.optinKeyword || 'TapOnIt';
    const clientHome = this.$appConfig('build').env.clientHome || 'http://www.taponitdeals.com';
    const phone = this.$appConfig('build').env.phone || '1-844-558-6766';
    const email = this.$appConfig('build').env.email || 'taponit@taponitdeals.com';
    const offerTypeDescLcase = this.$appConfig('build').env.offerTypeDescLcase || 'offer';
    const offerTypeDescUcase = this.$appConfig('build').env.offerTypeDescUcase || 'OFFER';
    const offerTypeDescPcase = this.$appConfig('build').env.offerTypeDescPLcase || 'Offer';
    return {
      subscriberGUID: '',
      searchParam: '',
      noOffersToRender: true,
      noSavedOffers: false,
      offersFiltered: [],
      savedOffers: [],
      savedOfferIDs: [],
      offers: [],
      allMarkets: [],
      allMarketIDs: '',
      city: +Cookie.get('selectedMarket') || 'all',
      state: '',
      offerSort: '',
      businessCategories: [],
      businessFilter: '',
      marketFilter: '',
      offersToRender: [],
      shortCode,
      optinKeyword,
      clientHome,
      phone,
      email,
      offerTypeDescLcase,
      offerTypeDescUcase,
      offerTypeDescPcase
    }
  },
  beforeDestroy() {
    Cookie.set('selectedMarket', this.city, { expires: 1 });
  },
  watch: {
    searchParam(val, oldVal) {
      if (val !== oldVal) {
        if (val !== null) {
          this.businessFilter = '';
        }
        this.offersFiltered = _.uniqBy([
          ...this.searchByValue(val, 'businessName'),
          ...this.searchByValue(val, 'businessCategories', true),
          ...this.searchByValue(val, 'name'),
          ...this.searchByValue(val, 'shortDescriptionText')
        ], 'offerID');
        this.offersToRender = _.cloneDeep(this.offersFiltered);
        if (this.offerSort && this.offerSort !== 'none') {
          this.changeOfferSortOrder();
        }
      }
    }
  },
  methods: {
    goTo(offer) {
      const url = '/offer/' + offer.offerGUID + '/' + this.subscriberGUID;
      this.$router.go(url);
    },
    searchByValue(searchTerm, category, searchInArray = false) {
      const value = searchTerm
        .replace(/[‘’]/g, '\'')
        .replace(/[“”]/g, '"');

      const searchValue = new RegExp(`.*${value}.*`, 'gi');

      if (!searchInArray) {
        return this.offers.filter(e => searchValue.test(e[category]));
      }
      return this.offers.filter(e => e[category] && e[category].some((c) => searchValue.test(c)));
    },
    changeOfferSortOrder() {
      if (this.offerSort === 'expired') {
        this.offersToRender = toiArrayHelper.sortBy(this.offersToRender, (x) => new Date(x.expirationDate));
      } else if (this.offerSort === 'name') {
        this.offersToRender = toiArrayHelper.sortBy(this.offersToRender, (x) => x.businessName);
      } else {
        this.offersToRender = _.cloneDeep(this.offersFiltered).filter(this.filterFunction);
      }
    },
    changeOfferFilter: function (value) {
      if (value !== null && this.searchParam !== '') {
        this.searchParam = null;
      }
      if (value) {
        if (this.businessFilter === value) {
          this.businessFilter = '';
        } else {
          this.businessFilter = value;
        }
      }

      this.offersToRender = this.offersFiltered.filter(this.filterFunction);
      if (this.offerSort && this.offerSort !== 'none') {
        this.changeOfferSortOrder();
      }
    },
    filterFunction(offer) {
      return this.businessFilter === '' ||
        (
          offer.businessCategories !== undefined &&
          offer.businessCategories.indexOf(this.businessFilter) > -1
        );
    },
    closeOverlay: function() {
      this.$dispatch('closeOverlay')
    },
    saveOffer: function(offer) {
      this.savedOffers.push({offerID: offer.offerID.toString(), marketID: offer.marketID.toString()})
      offer.saveOfferID = true
      Cookie.set('savedOffersMarkets', JSON.stringify(this.savedOffers), { expires: 365 })
      this.$http.post(this.$config('baseURL') + '/subscribers/saveOffer', {
        "offerGUID": offer.offerGUID,
        "subscriberGUID": this.subscriberGUID,
        "campaignID": 0
      }).then((response) => {
        this.savedOfferIDs.push(offer.offerID);
        if (this.noSavedOffers) {
          this.noSavedOffers = false;
          this.overlay.firstSavedOfferMessage = true;
        }
      });
    },
    getExpirationStatus(o) {
      return moment(o).diff(moment.now(), 'd');
    },
    calculateExpiringClass(offerDate) {
      const date = this.getExpirationStatus(offerDate);
      if (date <= 1) {
        return 'expiringtoday';
      } else if (date <= 7) {
        return 'expiringsoon';
      } else {
        return 'expiration';
      }
    },
    expirationText (offerDate) {
      const date = this.getExpirationStatus(offerDate);
      if (date < 0) {
        return 'Expired!';
      } else if (date <= 1) {
        return 'Expiring Today!';
      } else if (date <= 7) {
        return 'Expiring Soon!';
      } else {
        return 'Expires';
      }
    }
  },
  route: {
    canReuse: false,
    data: function() {
      // set subscriber GUID
      if (this.$route.params.subscriberGUID !== undefined && this.$route.params.subscriberGUID !== 'undefined') {
        this.subscriberGUID = this.$route.params.subscriberGUID
      } else if (Cookie.get('subscriberGUID') !== undefined) {
        this.subscriberGUID = Cookie.get('subscriberGUID')
      }

      this.$http.get(this.$config('baseURL') + '/subscribers/getAssignedMarkets?subscriberGUID=' + this.subscriberGUID).then(response => {
        let markets = JSON.parse(response.body).records

        this.allMarkets = markets.map(({ marketID, city, state }) => ({ marketID, city, state }));
        this.allMarketIDs = markets.map(market => market.marketID).join()
        if (!this.allMarkets.find(e => +e.marketID === +this.city) && this.city !== 'all') {
          this.city = 'all';
        }

        this.$http.get(this.$config('baseURL') + '/offers/getMoreOffers?subscriberGUID=' + this.subscriberGUID
          + '&marketID=' + this.allMarketIDs + '&showCurrentOnly=1').then((response) => {
          this.offers = _.uniqBy(JSON.parse(response.body).records, 'offerID');
          for (var i = 0; i < this.offers.length; i++) {
            this.offersToRender.push(this.offers[i]);
          }

          this.$http.get(this.$config('baseURL') + '/subscribers/getSubscriberBusinessCategories?subscriberGUID=' + this.subscriberGUID).then((response) => {
            //Get business categories and setup an index lookup
            var offerCategories = JSON.parse(response.body);
            var offerCategoryMerge = {};
            for (var x = 0; x < offerCategories.length; x++) {
              var k= offerCategories[x].BusinessId;
              if (offerCategoryMerge[k] === undefined) {
                offerCategoryMerge[k] = [];
              }
              offerCategoryMerge[k].push(offerCategories[x].Category);
            }

            //Assign business categories by index lookup
            for (var i = 0; i < this.offers.length; i++) {
              this.offers[i].businessCategories = offerCategoryMerge[this.offers[i].businessID];
            }

            //Get unique categories
            offerCategories = toiArrayHelper.sortBy(offerCategories, function(x) {return x.Category});

            var lastCategory = '';
            for (var i = 0; i < offerCategories.length; i++) {
              var v = offerCategories[i].Category;
              if (lastCategory != v) {
                lastCategory = v;
                this.businessCategories.push({id: v, text: v});
              }
            }
            this.NoOffersToRender = (this.offersToRender.length >=1) ? false : true
            this.$http.get(this.$config('baseURL') + '/subscribers/getSavedOffers?subscriberGUID=' + this.subscriberGUID + '&cache=' + Date.now()).then((response) => {
            this.savedOfferIDs = JSON.parse(response.body).records.map(offer => offer.offerID);
            this.offersFiltered = _.cloneDeep(this.offers);
            }, (response) => { })
          }, response => { })
        }, response => {
          this.noSubscriber = true
        });
      }, response => {
        this.NoOffersToRender = true
        this.noSavedOffers=true
      })
    }
  },
}
