import _ from 'lodash'
import Cookie from 'js-cookie'

export default {
  data () {
    const homeIconLink = this.$appConfig('build').env.homeIconLink || (this.$root.validMarket) ? '/' : '/ca/0/';
    const homeIconLinkNewTab = this.$appConfig('build').env.homeIconLinkNewTab ? '_blank' : '_self';
    return {
      homeIconLink,
      newDesign: this.$root.validMarket,
      homeIconLinkNewTab,
      legacy: false,
      overlay: {
        menu: false,
        modal: false
      },
      modalText: '',
      modalCb: null
    };
  },
  methods: {
    goTo(link) {
      this.closeOverlay();
      this.$router.go(link);
    },
    openOverlay: function(layer) {
      if (!this.cookiesDisabled) {
        this.closeOverlay();
        this.overlay[layer] = true;
      }
    },
    toggleOverlay: function(layer) {
      if (!this.cookiesDisabled) {
        this.overlay[layer] = !this.overlay[layer];
      }
    },
    openModal: function(modalText, modalCb) {
      if (!this.cookiesDisabled) {
        this.overlay.modal = true;
        this.modalText = modalText;
        this.modalCb = modalCb;
      }
    },
    closeOverlay() {
      Object.keys(this.overlay)
        .forEach((e) => {
          this.overlay[e] = false;
        });
    },
    confirmModal: function() {
      if (typeof this.modalCb === 'function') {
        this.modalCb();
      }
    },
    closeModal: function() {
      this.overlay.modal = false;
      this.modalCb = null;
    }
  },
  computed: {
    overlayOpened() {
      return Object.values(this.overlay).some(e => e) && this.$root.validMarket;
    },
    cookiesDisabled: function() {
	  return !("cookie" in document && (document.cookie.length > 0 || (document.cookie = "test").indexOf.call(document.cookie, "test") > -1))
    },
  },
  ready: function() {
    this.$on('closeOverlay', function() {
      this.closeOverlay()
      return true;
    })

    this.$on('openModal', function(modalText, modalCb) {
      this.openModal(modalText, modalCb);
      return true;
    });

    this.$on('closeModal', function() {
      this.closeModal();
      return true;
    });

    // set subscriber GUID
    if (this.$route.params.subscriberGUID !== undefined) {
      this.subscriberGUID = this.$route.params.subscriberGUID
    } else if (Cookie.get('subscriberGUID') !== undefined) {
      this.subscriberGUID = Cookie.get('subscriberGUID')
    }

    if (this.subscriberGUID === undefined) {
      this.legacy = false
    } else {
      var self = this

      this.$http.get(this.$config('baseURL') + '/subscribers/getAssignedMarkets?subscriberGUID=' + this.subscriberGUID).then(response => {
          if(_.find(JSON.parse(response.body).records, function(o) { return o.marketID > 5 && o.marketID < 11; })) {
            self.legacy = true
            Cookie.set('legacy', 1, { expires: 1 })
          } else {
            self.legacy = false
            Cookie.set('legacy', 0, { expires: 1 })
          }
      }, response => {
        // error
      })
    }
  }
}
