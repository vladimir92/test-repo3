import Cookie from 'js-cookie'
export default {
  data() {
    return {
      subscriber: { id: 0 },
      subscriberSwitchVal : '',
      subscriberOpen: false,
      server: {
        appEnvironment: 'unavailable',
        appVersion: '',
        appBranch: '',
        libVersion: '',
        libBranch: '',
        redis_url: '-',
        redis_port: '-',
        mssql_database: '-',
        mssql_url: '-',
        mssql_port: '-'
      },
      client: {
        url: this.$config('clientURL'),
        environment: this.$config('clientEnvironment'),
        version: this.$config('clientVersion'),
        baseURL: this.$config('baseURL')
      }
    }
  },
  methods: {
    getSubscriberGUID: function() {
      return Cookie.get('subscriberGUID') ||
        this.$route.params.subscriberGUID ||
        (this.$root.$children && this.$root.children[0] && this.$root.children[0].subscriberGUID)
        ||  false;
    },
    loadSubscriber: function() {
      if (this.getSubscriberGUID()) {
        if (this.$root.$children[0].subscriber && this.$root.$children[0].subscriber.subscriberGUID) {
          this.subscriber = this.$root.$children[0].subscriber;
        } else {
          this.$http.get(this.$config('baseURL') + '/subscribers/getByGUID?subscriberGUID=' + this.getSubscriberGUID())
            .then(response => {
              const subscriber = response.body ? JSON.parse(response.body).records[0] : null;
              if (subscriber) {
                this.$root.$children[0].subscriber = subscriber;
                this.subscriber = subscriber;
              }
            });
        }
      }
    },
    getSubscriberName: function() {
      return this.subscriber ?
        (this.subscriber.firstName ? this.subscriber.firstName + ' ' : '') +
        (this.subscriber.lastName ? this.subscriber.lastName : '')
        : '';
    },
    toggleSubscriber: function() {
      this.subscriberOpen = !this.subscriberOpen;
    },
    switchSubscriber: function() {
      let type = null;
      let url = this.$config('baseURL') + '/subscribers/';
      const switchVal = this.subscriberSwitchVal.trim();
      if (/^[\d]{11}$/.test(switchVal)) {
        type = 'number';
        url += 'getByNumber?number=' + switchVal;
      } else if (/^[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}$/.test(switchVal)) {
        type = 'guid';
        url += 'getByGUID?subscriberGUID=' + switchVal;
      } else if (/^\d+$/.test(switchVal)) {
        type = 'id';
        url += 'getById?id=' + switchVal;
      }
      if (!type) {
        alert('Invalid input type.  Must be an 11-digit phone number, 4+ digit ID, or a 32 character GUID');
      } else {
        this.$http.get(url).then(response => {
          const newSubscriber = response.body ? JSON.parse(response.body).records[0] : null;
          if (!newSubscriber) {
            alert(`The subscriber for ${type}: ${switchVal} was not found.`);
          } else {
            this.$root.$children[0].subscriber = newSubscriber;
            this.subscriber = newSubscriber;
            this.subscriberSwitchVal = '';
            Cookie.set('subscriberGUID', newSubscriber.subscriberGUID, { expires: 365 });
            window.location.href = '/sm/' + newSubscriber.subscriberGUID;
          }
        });

      }
    }
  },
  ready() {
    this.$http.get(this.$config('baseURL') + '/health').then(
      (response) => {
        if (response.json().status && response.json().status === 'ok') {
          this.server = response.json()
        }
      });
    this.loadSubscriber();
  }
}
