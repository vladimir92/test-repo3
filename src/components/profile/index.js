import Cookie from 'js-cookie';
import formsMixin from '../../mixins/forms.js';

export default {
  mixins: [formsMixin],
  data() {
    const shortCode = this.$appConfig('build').env.shortCode || '82928';
    const optinKeyword = this.$appConfig('build').env.optinKeyword || 'TapOnIt';
    const clientHome = this.$appConfig('build').env.clientHome || 'http://www.taponitdeals.com';
    const phone = this.$appConfig('build').env.phone || '1-844-558-6766';
    const email = this.$appConfig('build').env.email || 'taponit@taponitdeals.com';
    const offerTypeDescLcase = this.$appConfig('build').env.offerTypeDescLcase || 'offer';
    const offerTypeDescUcase = this.$appConfig('build').env.offerTypeDescUcase || 'OFFER';
    const offerTypeDescPcase = this.$appConfig('build').env.offerTypeDescPcase || 'Offer';
    return {
      src: '',
      pictureFile: null,
      shortCode,
      optinKeyword,
      clientHome,
      phone,
      email,
      offerTypeDescLcase,
      offerTypeDescUcase,
      offerTypeDescPcase,
      formOptions: {
        name: 'profile',
        resource: 'subscribers',
        httpAction: 'put'
      },
      form: {
        firstName: '',
        lastName: '',
        email: '',
        categories: [],
        zipCode: '',
        birthDay: '',
        birthMonth: '',
        birthYear: '',
        homeOwner: '',
        householdIncome: '',
        kids: '',
        gender: '',
        receiveEmails: '',
        dob: '',
        iconURL: '',
        mmsEnabled: ''
      },
      days: [{
        id: '01',
        text: '01'
      }, {
        id: '02',
        text: '02'
      },
      {
        id: '03',
        text: '03'
      }, {
        id: '04',
        text: '04'
      }, {
        id: '05',
        text: '05'
      }, {
        id: '06',
        text: '06'
      }, {
        id: '07',
        text: '07'
      }, {
        id: '08',
        text: '08'
      }, {
        id: '09',
        text: '09'
      }, {
        id: 10,
        text: '10'
      }, {
        id: 11,
        text: '11'
      }, {
        id: 12,
        text: '12'
      }, {
        id: 13,
        text: '13'
      }, {
        id: 14,
        text: '14'
      }, {
        id: 15,
        text: '15'
      }, {
        id: 16,
        text: '16'
      }, {
        id: 17,
        text: '17'
      }, {
        id: 18,
        text: '18'
      }, {
        id: 19,
        text: '19'
      }, {
        id: 20,
        text: '20'
      }, {
        id: 21,
        text: '21'
      }, {
        id: 22,
        text: '22'
      }, {
        id: 23,
        text: '23'
      }, {
        id: 24,
        text: '24'
      }, {
        id: 25,
        text: '25'
      }, {
        id: 26,
        text: '26'
      }, {
        id: 27,
        text: '27'
      }, {
        id: 28,
        text: '28'
      }, {
        id: 29,
        text: '29'
      }, {
        id: 30,
        text: '30'
      }, {
        id: 31,
        text: '31'
      }
      ],
      months: [{
        id: '01',
        text: 'January'
      }, {
        id: '02',
        text: 'February'
      }, {
        id: '03',
        text: 'March'
      }, {
        id: '04',
        text: 'April'
      }, {
        id: '05',
        text: 'May'
      }, {
        id: '06',
        text: 'June'
      }, {
        id: '07',
        text: 'July'
      }, {
        id: '08',
        text: 'August'
      }, {
        id: '09',
        text: 'September'
      }, {
        id: 10,
        text: 'October'
      }, {
        id: 11,
        text: 'November'
      }, {
        id: 12,
        text: 'December'
      }
      ],
      years: [{
        id: 2016,
        text: '2016'
      }, {
        id: 2015,
        text: '2015'
      }, {
        id: 2014,
        text: '2014'
      }, {
        id: 2013,
        text: '2013'
      }, {
        id: 2012,
        text: '2012'
      }, {
        id: 2011,
        text: '2011'
      }, {
        id: 2010,
        text: '2010'
      }, {
        id: 2009,
        text: '2009'
      }, {
        id: 2008,
        text: '2008'
      }, {
        id: 2007,
        text: '2007'
      }, {
        id: 2006,
        text: '2006'
      }, {
        id: 2005,
        text: '2005'
      }, {
        id: 2004,
        text: '2004'
      }, {
        id: 2003,
        text: '2003'
      }, {
        id: 2002,
        text: '2002'
      }, {
        id: 2001,
        text: '2001'
      }, {
        id: 2000,
        text: '2000'
      }, {
        id: 1999,
        text: '1999'
      }, {
        id: 1998,
        text: '1998'
      }, {
        id: 1997,
        text: '1997'
      }, {
        id: 1996,
        text: '1996'
      }, {
        id: 1995,
        text: '1995'
      }, {
        id: 1994,
        text: '1994'
      }, {
        id: 1993,
        text: '1993'
      }, {
        id: 1992,
        text: '1992'
      }, {
        id: 1991,
        text: '1991'
      }, {
        id: 1990,
        text: '1990'
      }, {
        id: 1989,
        text: '1989'
      }, {
        id: 1988,
        text: '1988'
      }, {
        id: 1987,
        text: '1987'
      }, {
        id: 1986,
        text: '1986'
      }, {
        id: 1985,
        text: '1985'
      }, {
        id: 1984,
        text: '1984'
      }, {
        id: 1983,
        text: '1983'
      }, {
        id: 1982,
        text: '1982'
      }, {
        id: 1981,
        text: '1981'
      }, {
        id: 1980,
        text: '1980'
      }, {
        id: 1979,
        text: '1979'
      }, {
        id: 1978,
        text: '1978'
      }, {
        id: 1977,
        text: '1977'
      }, {
        id: 1976,
        text: '1976'
      }, {
        id: 1975,
        text: '1975'
      }, {
        id: 1974,
        text: '1974'
      }, {
        id: 1973,
        text: '1973'
      }, {
        id: 1972,
        text: '1972'
      }, {
        id: 1971,
        text: '1971'
      }, {
        id: 1970,
        text: '1970'
      }, {
        id: 1969,
        text: '1969'
      }, {
        id: 1968,
        text: '1968'
      }, {
        id: 1967,
        text: '1967'
      }, {
        id: 1966,
        text: '1966'
      }, {
        id: 1965,
        text: '1965'
      }, {
        id: 1964,
        text: '1964'
      }, {
        id: 1963,
        text: '1963'
      }, {
        id: 1962,
        text: '1962'
      }, {
        id: 1961,
        text: '1961'
      }, {
        id: 1960,
        text: '1960'
      }, {
        id: 1959,
        text: '1959'
      }, {
        id: 1958,
        text: '1958'
      }, {
        id: 1957,
        text: '1957'
      }, {
        id: 1956,
        text: '1956'
      }, {
        id: 1955,
        text: '1955'
      }, {
        id: 1954,
        text: '1954'
      }, {
        id: 1953,
        text: '1953'
      }, {
        id: 1952,
        text: '1952'
      }, {
        id: 1951,
        text: '1951'
      }, {
        id: 1950,
        text: '1950'
      }, {
        id: 1949,
        text: '1949'
      }, {
        id: 1948,
        text: '1948'
      }, {
        id: 1947,
        text: '1947'
      }, {
        id: 1946,
        text: '1946'
      }, {
        id: 1945,
        text: '1945'
      }, {
        id: 1944,
        text: '1944'
      }, {
        id: 1943,
        text: '1943'
      }, {
        id: 1942,
        text: '1942'
      }, {
        id: 1941,
        text: '1941'
      }, {
        id: 1940,
        text: '1940'
      }, {
        id: 1939,
        text: '1939'
      }, {
        id: 1938,
        text: '1938'
      }, {
        id: 1937,
        text: '1937'
      }, {
        id: 1936,
        text: '1936'
      }, {
        id: 1935,
        text: '1935'
      }, {
        id: 1934,
        text: '1934'
      }, {
        id: 1933,
        text: '1933'
      }, {
        id: 1932,
        text: '1932'
      }, {
        id: 1931,
        text: '1931'
      }, {
        id: 1930,
        text: '1930'
      }, {
        id: 1929,
        text: '1929'
      }, {
        id: 1928,
        text: '1928'
      }, {
        id: 1927,
        text: '1927'
      }, {
        id: 1926,
        text: '1926'
      }, {
        id: 1925,
        text: '1925'
      }, {
        id: 1924,
        text: '1924'
      }, {
        id: 1923,
        text: '1923'
      }, {
        id: 1922,
        text: '1922'
      }, {
        id: 1921,
        text: '1921'
      }, {
        id: 1920,
        text: '1920'
      }, {
        id: 1919,
        text: '1919'
      }, {
        id: 1918,
        text: '1918'
      }, {
        id: 1917,
        text: '1917'
      }, {
        id: 1916,
        text: '1916'
      }, {
        id: 1915,
        text: '1915'
      }, {
        id: 1914,
        text: '1914'
      }, {
        id: 1913,
        text: '1913'
      }, {
        id: 1912,
        text: '1912'
      }, {
        id: 1911,
        text: '1911'
      }, {
        id: 1910,
        text: '1910'
      }, {
        id: 1909,
        text: '1909'
      }, {
        id: 1908,
        text: '1908'
      }, {
        id: 1907,
        text: '1907'
      }, {
        id: 1906,
        text: '1906'
      }, {
        id: 1905,
        text: '1905'
      }, {
        id: 1904,
        text: '1904'
      }, {
        id: 1903,
        text: '1903'
      }, {
        id: 1902,
        text: '1902'
      }, {
        id: 1901,
        text: '1901'
      }, {
        id: 1900,
        text: '1900'
      }
      ],
      incomes: [
        {
          id: 1,
          text: '$0-$24,999'
        },
        {
          id: 2,
          text: '$25,000-$49,999'
        },
        {
          id: 3,
          text: '$50,000-$74,999'
        },
        {
          id: 4,
          text: '$75,000-$99,999'
        },
        {
          id: 5,
          text: '$100,000-$124,999'
        },
        {
          id: 6,
          text: '$125,000-$149,999 '
        },
        {
          id: 7,
          text: '$150,000-$174,999'
        },
        {
          id: 8,
          text: '$175,000+'
        }
      ],
      categories: []
    };
  },
  computed: {
    classLabel() {
      return (this.$root.validMarket) ? 'input-label' : 'control-label';
    }
  },
  methods: {
    calcDob() {
      if (this.form.birthMonth && this.form.birthDay && this.form.birthYear) {
        this.form.dob = [
          `0${this.form.birthMonth}`.slice(-2),
          `0${this.form.birthDay}`.slice(-2),
          this.form.birthYear
        ].join('');
      } else {
        this.form.dob = null;
      }

      return this.form.dob;
    },
    uploadImage: function () {
      const file = this.pictureFile;
      if (!file || !file.name) {
        return Promise.resolve();
      }

      const filename = Date.now() + file.name.replace(/[^A-Z0-9.]/gi, '');
      const getSignedURL = this.$config('baseURL') +
        `/files/getSignedURL?fileName=${filename}&contentType=${file.type}`;
      const compressImageURL = this.$config('baseURL') + '/files/compressImage';
      let url = '';
      return this.$http.get(getSignedURL)
        .then((response) => response.json())
        .then((response) => {
          url = `${response.url.split('/').slice(0, 3).join('/')}/${filename}`;
          return this.$http.put(response.url, file, { headers: { 'Content-Type': file.type } });
        })
        .then(() => this.$http.post(compressImageURL, { url }))
        .then((response) => response.json())
        .then((response) => response.url)
        .catch((response) => console.warn('The file wasn\'t saved.', response));
    },
    preSubmitForm(event) {
      if (this.loading) {
        return;
      }
      this.calcDob();

      this.loading = true;
      if (this.form.iconURL !== this.src) {
        return this.uploadImage()
          .then((iconURL) => {
            if (iconURL) {
              this.src = iconURL;
              this.form.iconURL = iconURL;
            }
            return this.submitForm(this.formOptions, event);
          })
          .catch(() => this.submitForm(this.formOptions, event));
      }

      this.form.categories = this.categories
        .filter((e) => e.checked)
        .map((e) => e.businessCategoryID);

      return this.submitForm(this.formOptions, event);
    },
    updateCategory(category) {
      category.checked = !category.checked;
    },
    updateCheckbox(value, key) {
      this.form[key] = value;
    },
    triggerEvent() {
      document.getElementById('input').click();
    },
    imageOnChange(event) {
      const reader = new window.FileReader();
      reader.onload = (e) => {
        this.src = e.target.result;
      };
      this.pictureFile = event.target.files[0];
      reader.readAsDataURL(event.target.files[0]);
    },

    closeOverlay: function () {
      this.$dispatch('closeOverlay');
    },
    omg: function (e) {
      console.log(e);
    }
  },
  ready: function () {
    this.loading = true;
    // get or set saved offers cookie
    this.subscriberGUID = Cookie.get('subscriberGUID');

    if (!this.subscriberGUID) {
      // send to help landing page
      this.form = {};
      console.warn('subscriberGUID not found');
    } else {
      this.$http.get(this.$config('baseURL') + '/subscribers/getByGUID?subscriberGUID=' + this.subscriberGUID)
        .then((response) => {
          this.form = JSON.parse(response.body).records[0];
          ['kids', 'mmsEnabled', 'gender']
            .filter((e) => ['number', 'boolean', 'string'].includes(typeof this.form[e]))
            .forEach((e) => {
              this.form[e] = +this.form[e];
            });

          this.src = this.form.iconURL;
          if (this.form.mmsEnabled == null) {
            this.form.mmsEnabled = false;
          }
          this.categories.forEach((e) => {
            e.checked = this.form.categories.includes(e.businessCategoryID);
          });
          if (JSON.parse(response.body).records[0].dob !== null) {
            this.form.birthMonth = JSON.parse(response.body).records[0].dob.slice(0, 2);
            this.form.birthDay = JSON.parse(response.body).records[0].dob.slice(2, 4);
            this.form.birthYear = JSON.parse(response.body).records[0].dob.slice(4, 8);
          }

          this.loading = false;
        }, () => {
          // error
          this.loading = false;
        });
    }
    if (this.$root.validMarket) {
      this.$http.get(this.$config('baseURL') + '/businessCategory/getAllActive')
        .then(response => response.json())
        .then((response) => {
          this.categories = response.records
            .map((e) => ({ ...e, checked: this.form.categories.includes(e.businessCategoryID) }));
        });
    }
  }
};
