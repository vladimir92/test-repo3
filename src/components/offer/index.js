// this is the page that is reached when a user clicks on a shared link.
import Cookie from 'js-cookie';

export default {
  data () {
    const shortCode = this.$appConfig('build').env.shortCode || '82928';
    const optinKeyword = this.$appConfig('build').env.optinKeyword || 'TapOnIt';
    const clientHome = this.$appConfig('build').env.clientHome || 'http://www.taponitdeals.com';
    const phone = this.$appConfig('build').env.phone || '1-844-558-6766';
    const email = this.$appConfig('build').env.email || 'taponit@taponitdeals.com';
    const offerTypeDescLcase = this.$appConfig('build').env.offerTypeDescLcase || 'offer';
    const offerTypeDescUcase = this.$appConfig('build').env.offerTypeDescUcase || 'OFFER';
    const offerTypeDescPcase = this.$appConfig('build').env.offerTypeDescPcase || 'Offer';
    return {
      analyticsData: {
        eventCategory: 'OptinForm',
        eventAction: 'submitMSISDN'
      },
      noOffer: null,
      offer: {},
      number: null,
      numberRequired: null,
      submitted: false,
      alreadySubscribed: false,
      invalidNumber: false,
      shortCode,
      optinKeyword,
      clientHome,
      phone,
      email,
      offerTypeDescLcase,
      offerTypeDescUcase,
      offerTypeDescPcase
    }
  },
  methods: {
    optIn: function (e) {
      e.preventDefault();
      let subscriberGUID = this.$route.params.subscriberGUID
        ? this.$route.params.subscriberGUID
        : Cookie.get('subscriberGUID');

      if (subscriberGUID === null || subscriberGUID === 'null') { // Because subscriberGUID can apparently also be a string...
        subscriberGUID = 'd39554f9-935b-4864-8459-7ffa0d724de3'; // Test Subscriber #1
      }

      const campaignID = this.$route.params.campaignID ? this.$route.params.campaignID : null;

      if (this.number) {
        this.numberRequired = false;

        this.$ga.trackEvent(this.analyticsData.eventCategory, this.analyticsData.eventAction);
        var saveRoute = this.$config('baseURL') + '/subscribers/save?subscriberGUID=' + subscriberGUID
          + '&offerGUID=' + this.$route.params.offerGUID
          + (campaignID ? '&campaignID=' + campaignID : '')
          + '&number=' + this.number
          + '&shortCode=' + this.shortCode
          + '&referrer=' + window.document.referrer;
        this.$http.post(saveRoute).then((response) => {
          if (response) {
            if (response.body == '"alreadySubscribed"') {
              this.alreadySubscribed = true
            } else if (response.body =='"InvalidPhoneNumber"') {
              this.invalidNumber = true
            } else {
              this.submitted = true
            }
          }
        })
      } else {
        this.numberRequired = true
      }
    },
  },
  route: {
    canReuse: false,
    data: function() {
      if (this.$route.params.offerGUID) {
        this.$http.get(this.$config('baseURL') + '/offers/getByGUID?offerGUID=' + this.$route.params.offerGUID).then((response) => {
          var res = JSON.parse(response.body);
          if (res.records && res.records.length > 0) {
            this.offer = res && res.records && res.records.length > 0 ? res.records[0] : {};
            this.$emit('updateHead')

            // store referrer
            let referrer = Cookie.get('referrer')

            this.$http.post(this.$config('baseURL') + '/metrics/offerClick', {
              "url": window.location.href,
              "referrer": referrer || window.document.referrer,
              "marketID": this.offer.marketID,
              "offerID": this.offer.offerID,
              "subscriberGUID": this.$route.params.subscriberGUID
            }).then((response) => {
              Cookie.remove('referrer')
            })
          } else {
            this.noOffer = true;
          }

        })
      } else {
        this.noOffer = true
      }
    }
  },
  head: {
    title: function () {
      return {
        inner: this.offer && this.offer.name ? this.offer.name : '',
        separator: '|',
        complement: `${this.$root.$config('siteName')}`
      }
    },
    meta: function () {
      var offerName = this.offer && this.offer.name ? this.offer.name.replace(/\d+\/\d+\/\d+$/, "")  : "";
      const subscriberGUIDPath = this.$route.params.subscriberGUID ? `/${this.$route.params.subscriberGUID}` : '';
      const campaignIDPath = this.$route.params.campaignID ? `/${this.$route.params.campaignID}` : '';
      const site = `${window.location.origin}/o/${this.offer.offerGUID}${subscriberGUIDPath}${campaignIDPath}`
      return [
        { n: 'twitter:card', c: 'summary_large_image' },
        { n: 'twitter:site', c: site },
        { n: 'twitter:title', c: offerName },
        { n: 'twitter:description', c: `An offer from ${this.$root.$config('siteName')}!` },
        { n: 'twitter:image', c: this.offer.image },
        { p: 'fb:app_id', c: this.$root.$config('facebookApiID') },
        { p: 'og:url', c: site },
        { p: 'og:site_name', c: '82928' },
        { p: 'og:title', c: offerName },
        { p: 'og:type', c: 'website' },
        { p: 'og:description', c: `An offer from ${this.$root.$config('siteName')}!` },
        { p: 'og:image', c: this.offer.image },
        { p: 'og:image:width', c: '500' },
        { p: 'og:image:height', c: '383' }
      ]
    }
  }
}
