import Cookie from 'js-cookie'
import _ from 'lodash'
import toiArrayHelper from '../../assets/scripts/array-helpers.js'
import moment from 'moment'

export default {
  data () {
    const offerTypeDescLcase = this.$appConfig('build').env.offerTypeDescLcase || 'offer';
    const offerTypeDescUcase = this.$appConfig('build').env.offerTypeDescUcase || 'OFFER';
    const offerTypeDescPcase = this.$appConfig('build').env.offerTypeDescPLcase || 'Offer';
    return {
      subscriberGUID: '',
      allMarkets: [],
      allMarketIDs: '',
      noSavedOffers: false,
      city: 'all',
      state: '',
      offers: [],
      offerSort: '',
      businessCategories: [],
      businessFilter: '',
      offersToRender: [],
      offerTypeDescLcase,
      offerTypeDescUcase,
      offerTypeDescPcase
    }
  },
  computed: {
    calculateClass() {
      if (this.$root.validMarket) {
        return 'star';
      }
      return 'button delete';
    }
  },
  methods: {
    goTo(offer) {
      this.closeOverlay();
      return this.$router.go('/offer/' + offer.offerGUID + '/' + this.subscriberGUID);
    },
    changeOfferSortOrder() {
      if (this.offerSort === 'expired') {
        this.offersToRender = toiArrayHelper.sortBy(this.offersToRender, (x) => new Date(x.expirationDate));
      } else if (this.offerSort === 'name') {
        this.offersToRender = toiArrayHelper.sortBy(this.offersToRender, (x) => x.businessName);
      } else {
        this.offersToRender = _.cloneDeep(this.offers);
      }
    },
    closeOverlay: function() {
      this.$dispatch('closeOverlay')
    },
    closeModal: function() {
      this.$dispatch('closeModal');
    },
    confirmRemoveSavedOffer: function(id, guid, campaignID) {
      this.$dispatch('openModal', `Are you sure you want to remove this offer from Favorites?`, () => {
        this.$http.delete(
          this.$config('baseURL') + '/subscribers/removeOfferSave?offerGUID=' + guid +
          '&subscriberGUID=' + this.subscriberGUID + '&campaignID=' + campaignID
        ).then(() => {
          const [deleted] = this.offersToRender.splice(id, 1);
          const deletedIndex = this.offers.findIndex((e) => e.offerGUID === deleted.offerGUID);
          this.offers.splice(deletedIndex, 1);
          this.closeModal();
        });
      });
    },
    getExpirationStatus: function(o){
      let now=moment();
      let expiration=moment(o)
      let status;
      if (now>expiration) {return 'Expired'}
        else if (expiration.add(-7,'day')<now){return 'Expiring Soon' }
        else {return 'Expires'}
    },
  },
  ready: function() {
    // get or set saved offers cookie
    this.subscriberGUID = Cookie.get('subscriberGUID')

    // add timestamp to break cache
    this.$http.get(this.$config('baseURL') + '/subscribers/getAssignedMarkets?subscriberGUID=' + this.subscriberGUID).then(response => {
      let markets = JSON.parse(response.body).records
      this.allMarkets = markets.map(market => { return {'marketID': market.marketID, 'city': market.city, 'state': market.state} })
      this.allMarketIDs = markets.map(market => market.marketID).join();
      this.$http.get(this.$config('baseURL') + '/subscribers/getSavedOffers?subscriberGUID=' +
        this.subscriberGUID + '&marketID=' + this.allMarketIDs + '&cache=' + Date.now())
        .then((response) => {
          this.offers = JSON.parse(response.body).records;

          this.noSavedOffers = this.offers.length == 0;

          this.$http.get(this.$config('baseURL') + '/subscribers/getSubscriberBusinessCategories?subscriberGUID=' + this.subscriberGUID).then((response) => {
            //Get business categories and setup an index lookup
            var offerCategories = JSON.parse(response.body);
            var offerCategoryMerge = {};
            for (var x = 0; x < offerCategories.length; x++) {
              var k= offerCategories[x].BusinessId;
              if (offerCategoryMerge[k] === undefined) {
                offerCategoryMerge[k] = [];
              }
              offerCategoryMerge[k].push(offerCategories[x].Category);
            }

            //Assign business categories by index lookup
            offerCategories = [];
            for (var i = 0; i < this.offers.length; i++) {
              this.offers[i].businessCategories = offerCategoryMerge[this.offers[i].businessID];

              var categories = offerCategoryMerge[this.offers[i].businessID];
              if (categories != null) {
                for (var c = 0; c < categories.length; c++) {
                  offerCategories.push(categories[c]);
                }
              }
            }

            //Get unique categories
            offerCategories = toiArrayHelper.sortBy(offerCategories, function(x) {return x});

            var lastCategory = '';
            for (var i = 0; i < offerCategories.length; i++) {
              var v = offerCategories[i];
              if (lastCategory != v) {
                lastCategory = v;
                this.businessCategories.push({id: v, text: v});
              }
            }
            this.offersToRender = _.cloneDeep(this.offers);
          }, response => { });
        }, (response) => {
          this.noSavedOffers = true
        });
    });
  },
  route: {
    canReuse: false
  }
}
