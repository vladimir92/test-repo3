import Cookie from 'js-cookie'
import toiArrayHelper from '../../assets/scripts/array-helpers.js'
import _ from 'lodash'

export default {
  data () {
    return {
      errorMessage: '',
      markets: [],
      saved: false,
      error:false,
      subscriberGUID: ''
    }
  },
  computed: {},
  methods: {
    toggleSelected: function(e) {
      window.test = this.marketData;
      let id = _.isEmpty(event.target.id) ? $(event.target).closest('a')[0].id : event.target.id;
      id = parseInt(id.substring(id.lastIndexOf('-') + 1));
      var market = _.find(this.markets, ['marketID', id]);
      market.selected = !market.selected;
      market.modified = market.selected !== market.originalSelected;
    },
    saveMarkets: function() {
      if (!this.saved) {
        this.$http.post(
          this.$config('baseURL') + '/markets/optInMarkets', {
            "subscriberGUID": this.subscriberGUID,
            "markets": this.markets
          }).then(() => this.$root.evaluateMarkets())
          .then((valid) => {
            this.$root.newDesign = valid;
            this.saved = true
            this.error =false
            this.markets.forEach(market => {
              market.modified = false;
              market.originalSelected = market.selected;
            });
            const path = (this.$root.validMarket && '/') || '/ca/0';
            this.$router.go(path);
          }).catch(() => {
            this.saved = false
            this.error = true
          });
      }
    }
  },
  route: {
    data: function() {
      // set subscriber GUID
      if (this.$route.params.subscriberGUID !== 'undefined' && this.$route.params.subscriberGUID !== undefined) {
        this.subscriberGUID = this.$route.params.subscriberGUID
        Cookie.set('subscriberGUID', this.subscriberGUID, { expires: 365 })
      } else if (Cookie.get('subscriberGUID') !== undefined) {
        this.subscriberGUID = Cookie.get('subscriberGUID')
      }

      // get or set saved offers cookie
      // clear saved offers for switch to different syntax
      if (Cookie.get('savedOffersMarkets') === undefined) {
        Cookie.set('savedOffersMarkets', '[]', { expires: 365 })
      }

      this.$http.get(this.$config('baseURL') + '/markets/getAllActive?cache=' + Date.now()).then((response) => {
        const allMarkets  = JSON.parse(response.body).records;
        const isProduction = ['stage', 'production'].includes(process.env.NODE_ENV);
        const filteredMarkets = toiArrayHelper.sortBy(allMarkets.filter(am => (!am.isTestMarket || !isProduction)), x => x.city);

        this.$http.get(this.$config('baseURL') + '/subscribers/getAssignedMarkets?subscriberGUID=' + this.subscriberGUID).then(response => {
          var m = JSON.parse(response.body).records
          var a = []
          for (var i = 0; i < m.length; i++) {
            a.push(m[i].marketID)
          }

          for (var i = 0; i < filteredMarkets.length; i++) {
            if (a.indexOf(filteredMarkets[i].marketID) > -1) {
              filteredMarkets[i].selected = true
            }
            else {
              filteredMarkets[i].selected = false
            }
            filteredMarkets[i].originalSelected = filteredMarkets[i].selected;
          }

          this.markets = filteredMarkets;
        })
      })

    }
  }
}
