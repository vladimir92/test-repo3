import Cookie from 'js-cookie';

export default {
  data() {
    return {
      error: false
    };
  },
  route: {
    data() {
      if (this.$route.params.shortURL) {
        let self = this;
        this.$http.get(this.$config('baseURL') + '/shortURL/getURL?code=' + this.$route.params.shortURL)
          .then((response) => response.json())
          .then((response) => {
            Cookie.set('referrer', window.document.referrer);

            if (response == null || [null, undefined, ''].includes(response.targetURL)) {
              self.error = true;

              Cookie.set('targetURL', window.location.origin + '/ca/0');
              this.$router.go('/ca/0?shortURL=' + self.$route.params.shortURL);
            } else {
              Cookie.set('targetURL', response.targetURL);
              if (/\/ca\//g.test(response.targetURL)) {
                const [campaignID, subscriberGUID, marketID] = response.targetURL.split(/\/ca\//)[1].split(/\//);
                Cookie.set('lastCampaignID', campaignID);
                Cookie.set('subscriberGUID', subscriberGUID);
                if (!isNaN(marketID)) {
                  Cookie.set('lastMarketID', marketID);
                } else {
                  Cookie.set('lastMarketID', null);
                }
                this.$root.addToLocalStorage();
                this.$root.createManifest();
                return this.$root.evaluateMarkets()
                  .then((valid) => {
                    if (valid) {
                      return this.$router.go('/');
                    }
                    window.location.href = response.targetURL + '?shortURL=' + self.$route.params.shortURL;
                  });
              }
              window.location.href = response.targetURL + '?shortURL=' + self.$route.params.shortURL;
            }
          });
      } else if (this.$route.params.previewCode) {
        Cookie.set('referrer', window.document.referrer);
        this.$router.go('/ca/preview/' + this.$route.params.previewCode);
      } else {
        this.error = true;
        this.$router.go('/ca/0?shortURL=' + this.$route.params.shortURL);
      }
    }
  }
};
