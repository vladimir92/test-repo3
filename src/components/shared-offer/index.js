import Cookie from 'js-cookie'

export default {
  data () {
    const shortCode = this.$appConfig('build').env.shortCode || '82928';
    const optinKeyword = this.$appConfig('build').env.optinKeyword || 'TapOnIt';
    const clientHome = this.$appConfig('build').env.clientHome || 'http://www.taponitdeals.com';
    const phone = this.$appConfig('build').env.phone || '1-844-558-6766';
    const email = this.$appConfig('build').env.email || 'taponit@taponitdeals.com';
    const offerTypeDescLcase = this.$appConfig('build').env.offerTypeDescLcase || 'offer';
    const offerTypeDescUcase = this.$appConfig('build').env.offerTypeDescUcase || 'OFFER';
    const offerTypeDescPcase = this.$appConfig('build').env.offerTypeDescPcase || 'Offer';
    return {
      overlay: {
        redeem: false
      },
      noOffer: null,
      offer: {},
      shortCode,
      optinKeyword,
      clientHome,
      phone,
      email,
      offerTypeDescLcase,
      offerTypeDescUcase,
      offerTypeDescPcase
    }
  },
  methods: {
    redeemOffer: function() {
      this.$http.post(this.$config('baseURL') + '/offers/redeem', {
        "campaignID": this.$route.params.campaignID,
        "offerID": this.offers[this.currentOffer].offerID,
        "subscriberGUID": this.$route.params.subscriberGUID
      }).then((response) => {
          this.redeemed[this.currentOffer] = true
          this.$http.get(this.$config('baseURL') + '/offers/getByGUID?offerGUID=' + this.$route.params.offerGUID).then((response) => {
          this.offer = JSON.parse(response.body).records[0]
        })
      })
    }
  },
  route: {
    data: function() {
      if (this.$route.params.offerGUID) {
        this.$http.post(this.$config('baseURL') + '/offers/bonusOfferAllowed', {
          "offerGUID": this.$route.params.offerGUID,
          "subscriberGUID": this.$route.params.subscriberGUID
        }).then((response) => {
          this.offer = JSON.parse(response.body).records[0]

          // store referrer
          let referrer = Cookie.get('referrer')

          this.$http.post(this.$config('baseURL') + '/metrics/offerClick', {
            "url": window.location.href,
            "referrer": referrer || window.document.referrer,
            "marketID": this.offer.marketID,
            "offerID": this.offer.offerID,
            "subscriberGUID": this.$route.params.subscriberGUID
          }).then((response) => {
            Cookie.remove('referrer')
          })
        })
      } else {
        this.noOffer = true
      }
    }
  },
  head: {
    title: function () {
      return {
        inner: this.offer.name || '',
        separator: '|',
        complement: `${this.$root.$config('siteName')}`
      }
    }
  }
}
