import Cookie from 'js-cookie';
import moment from 'moment/moment';
import _ from 'lodash';

export default {
  data() {
    const imagePlaceholder = 'https://via.placeholder.com/150x150';
    const shortCode = this.$appConfig('build').env.shortCode || '82928';
    const optinKeyword = this.$appConfig('build').env.optinKeyword || 'TapOnIt';
    const clientHome = this.$appConfig('build').env.clientHome || 'http://www.taponitdeals.com';
    const phone = this.$appConfig('build').env.phone || '1-844-558-6766';
    const email = this.$appConfig('build').env.email || 'taponit@taponitdeals.com';
    const siteName = this.$appConfig('build').env.siteName || 'TapOnIt';
    const offerTypeDescLcase = this.$appConfig('build').env.offerTypeDescLcase || 'offer';
    const offerTypeDescUcase = this.$appConfig('build').env.offerTypeDescUcase || 'OFFER';
    const offerTypeDescPcase = this.$appConfig('build').env.offerTypeDescPcase || 'Offer';
    const subscriberGUID = Cookie.get('subscriberGUID');
    const campaignID = Cookie.get('lastCampaignID');
    const marketID = Cookie.get('lastMarketID');
    const socialMedia1 = this.$appConfig('build').env.socialMedia1 || 'https://www.facebook.com/taponitmobiledeals'
    const socialMedia2 = this.$appConfig('build').env.socialMedia2 || 'https://twitter.com/TapOnItDeals'
    return {
      imagePlaceholder,
      campaignID,
      marketID,
      overlay: false,
      shortCode,
      optinKeyword,
      clientHome,
      siteName,
      phone,
      email,
      socialMedia1,
      socialMedia2,
      offerTypeDescLcase,
      offerTypeDescUcase,
      offerTypeDescPcase,
      subscriberGUID,
      offersReady: false,
      offersToRender: []
    };
  },
  methods: {
    triggerGeolocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(this.showPosition, this.failed);
      } else {
        console.error('Geolocation is not supported by this browser.');
      }
    },
    failed(error) {
      console.error(error.message);
      this.closeOverlay();
    },
    showPosition() {
      this.closeOverlay();
    },
    closeOverlay() {
      this.overlay = false;
    },
    clicked(offerClicked) {
      if (offerClicked.expired) {
        return;
      }
      const route = `/offer/${offerClicked.offerGUID}/${this.subscriberGUID}`;
      this.$router.go(route);
    },
    goTo(route) {
      this.$router.go(route);
    },
    getExpirationStatus(o, now) {
      const expiration = moment(o);
      if (now > expiration) {
        return -1;
      } else if (expiration.add(-3, 'day') < now) {
        return 0;
      } else {
        return 1;
      }
    },
    getAllData() {
      const url = [
        `campaignID=${this.campaignID}`,
        `subscriberGUID=${this.subscriberGUID}`,
        `marketID=${this.marketID}`
      ].join('&');

      return this.$http.get(this.$config('baseURL') + '/offers/getBySubscriberAndCampaign?' + url)
        .then((response) => response.json())
        .then((response) => {
          const now = moment();
          this.offersReady = true;
          this.offersToRender = _.uniqBy(response.records, 'offerID')
            .map((item) => ({
              ...item,
              expirationStatus: this.getExpirationStatus(item.expirationDate, now)
            }));
        })
        .catch(() => {
          this.offersReady = true;
        });
    }
  },
  route: {
    canReuse: false,
    activate(transition) {
      if (navigator.permissions) {
        navigator.permissions.query({ name: 'geolocation' })
          .then(permission => {
            this.overlay = permission.state === 'prompt';
          });
      } else {
        console.error('Geolocation is not supported by this browser.');
      }

      let hash = window.location.hash.match(/#!(.*)/);
      if (hash !== null && !['/', undefined].includes(hash[1])) {
        return transition.next(hash[1]);
      } else {
        transition.next();
      }

      if (this.$root.validMarket) {
        this.getAllData();
      }
    }
  }
};
