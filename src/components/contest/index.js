import Cookie from 'js-cookie'

export default {
  route: {
    canReuse: false,
    data: function() {
      if (this.$route.params.marketID !== undefined) {
        this.$http.get(this.$config('baseURL') + '/contest/getByMarketID?marketID=' + this.$route.params.marketID).then((response) => {
          const body = JSON.parse(response.body);
          if (body.metadata.rowcount === 0) {
            this.expired = true
            this.ready = true
          } else {
            this.contest = body.records[0]
            this.ready = true
            if (Cookie.get('contest' + this.contest.contestID) !== undefined) {
              this.submitted = true
              this.repeatEntry = true
            }
          }
        })
      } else {
        this.$http.get(this.$config('baseURL') + '/contest/search?urlToken=' + this.$route.params.urlToken).then((response) => {
          if (JSON.parse(response.body).metadata.rowcount === 0) {
            this.expired = true
            this.ready = true
          } else {
            this.contest = JSON.parse(response.body).records[0]
            this.ready = true

            if (Cookie.get('contest' + this.contest.contestID) !== undefined) {
              this.submitted = true
              this.repeatEntry = true
            }
          }
        })
      }

      var subscriberGUID = Cookie.get('subscriberGUID')
      if (subscriberGUID){
        this.$http.get(this.$config('baseURL') + '/contest/contestantInfo?subscriberGUID=' + subscriberGUID).then((response) => {
          const contestantInfo = JSON.parse(response.body).records[0];
          this.contestantInfo = this.createContestantInfo(contestantInfo, this.subscriber);
        });

        this.getSubscriberInfo(() => {
          if (this.subscriber && this.subscriber.mostRecentCampaign ) {
              this.menulink = '/ca/' + this.subscriber.mostRecentCampaign + '/' + subscriberGUID
            }
        })
      }
    }
  },
  events: {
    'vue-formular.sent': function(response) {
      this.submitted = true

      Cookie.set('contest' + this.contest.contestID, true, { expires: 365 })

      response = JSON.parse(response.body)

      if (response[1].records[0].updated > 0) {
        this.repeatEntry = true
      }
    },
    'vue-formular.invalid.server': function(response) {
      response = JSON.parse(response.body)

      if (response.errors.hasOwnProperty('subscriber')) {
        this.$children[0].
          this.mobileNumberError = false
        this.subscriberError = true
      }

      if (response.errors.hasOwnProperty('mobileNumber')) {
        this.subscriberError = false
        this.mobileNumberError = true
      }
    },
    'vue-formular.change::mobileNumber': function(newVal) {
      this.subscriberError = false;
      let ph = newVal.value.replace(/[^0-9]/g, "");

      if (newVal.value.length === 10) {
        if (ph.length === 10) {
          ph = '1' + ph;
        }
      }
      if ((ph.substring(0, 1) === '1') && (ph.length === 11)) {
          console.log(ph);
          this.$http.get(this.$config('baseURL') + '/subscribers/getByNumberMarketID?number=' + ph + '&marketID=' + this.contest.marketID).then((response) => {
          const subscriberInfo = JSON.parse(response.body).records[0];
          if (subscriberInfo) {
            this.contestantInfo = this.createContestantInfo(this.contestantInfo, subscriberInfo);
            Cookie.set('subscriberGUID', subscriberInfo.subscriberGUID, { expires: 365 });
          } else {
            this.subscriberError = true;
          }
        });
      } else {
        this.subscriberError = true;
      }
    }
  },
  computed: {
    rulesLink: function() {
      if (this.$route.params.marketID) {
        return '/static/pdf/contest-rules-' + this.$route.params.marketID + '.pdf'
      } else {
        return this.contest.rulePDF
      }
    },
    formURL: function() {
      return this.$config('baseURL') + '/contestEntry/save?contestID=' + this.contest.contestID
    }
  },
  data () {
    const shortCode = this.$appConfig('build').env.shortCode || '82928';
    const optinKeyword = this.$appConfig('build').env.optinKeyword || 'TapOnIt';
    const clientHome = this.$appConfig('build').env.clientHome || 'http://www.taponitdeals.com';
    const phone = this.$appConfig('build').env.phone || '1-844-558-6766';
    const email = this.$appConfig('build').env.email || 'taponit@taponitdeals.com';
    const offerTypeDescLcase = this.$appConfig('build').env.offerTypeDescLcase || 'offer';
    const offerTypeDescUcase = this.$appConfig('build').env.offerTypeDescUcase || 'OFFER';
    const offerTypeDescPcase = this.$appConfig('build').env.offerTypeDescPcase || 'Offer';
    return {
      contest: null,
      contestName: '',
      showOverlay: false,
      submitted: false,
      mobileNumberError: false,
      subscriberError: false,
      repeatEntry: false,
      expired: false,
      ready: false,
      shortCode,
      optinKeyword,
      clientHome,
      phone,
      email,
      offerTypeDescLcase,
      offerTypeDescUcase,
      offerTypeDescPcase,
      contestantInfo: {
        mobileNumber: '',
        firstName: '',
        lastName: '',
        email: '',
        gender: '',
        zipCode: '',
        dobd: '',
        dobm: '',
        doby: ''
      },
      subscriber: {},
      menulink: '/ca/0/',
      validation: {
        rules: {
          'mobileNumber': {
            required: true
          },
          'firstName': {
            required: true
          },
          'lastName': {
            required: true
          },
          email: {
            required: true
          },
          gender: {
            required: true
          },
          zipCode: {
            required: true
          },
          'birthdayDay': {
            required: true
          },
          'birthdayMonth': {
            required: true
          },
          'birthdayYear': {
            required: true
          },
          homeOwner: {
            required: true
          },
          'householdIncome': {
            required: false
          },
          kids: {
            required: false
          },
          agreeToTerms: {
            required: true
          }
        }
      },
      options: {
        customRules: {
          checkedRequired: function(field) {
            return field.checked === true
          }
        },
        messages: {
          required: 'This is required.',
          checkedRequired: 'You must agree to the terms & conditions.'
        },
        additionalPayload: { contestURL: this.$route.params.market }
      },
      days: [{
        id: '01',
        text: '01'
      }, {
        id: '02',
        text: '02'
      }, {
        id: '03',
        text: '03'
      }, {
        id: '04',
        text: '04'
      }, {
        id: '05',
        text: '05'
      }, {
        id: '06',
        text: '06'
      }, {
        id: '07',
        text: '07'
      }, {
        id: '08',
        text: '08'
      }, {
        id: '09',
        text: '09'
      }, {
        id: 10,
        text: '10'
      }, {
        id: 11,
        text: '11'
      }, {
        id: 12,
        text: '12'
      }, {
        id: 13,
        text: '13'
      }, {
        id: 14,
        text: '14'
      }, {
        id: 15,
        text: '15'
      }, {
        id: 16,
        text: '16'
      }, {
        id: 17,
        text: '17'
      }, {
        id: 18,
        text: '18'
      }, {
        id: 19,
        text: '19'
      }, {
        id: 20,
        text: '20'
      }, {
        id: 21,
        text: '21'
      }, {
        id: 22,
        text: '22'
      }, {
        id: 23,
        text: '23'
      }, {
        id: 24,
        text: '24'
      }, {
        id: 25,
        text: '25'
      }, {
        id: 26,
        text: '26'
      }, {
        id: 27,
        text: '27'
      }, {
        id: 28,
        text: '28'
      }, {
        id: 29,
        text: '29'
      }, {
        id: 30,
        text: '30'
      }, {
        id: 31,
        text: '31'
      }],
      months: [ {
        id: '01',
        text: 'January'
      }, {
        id: '02',
        text: 'February'
      }, {
        id: '03',
        text: 'March'
      }, {
        id: '04',
        text: 'April'
      }, {
        id: '05',
        text: 'May'
      }, {
        id: '06',
        text: 'June'
      }, {
        id: '07',
        text: 'July'
      }, {
        id: '08',
        text: 'August'
      }, {
        id: '09',
        text: 'September'
      }, {
        id: 10,
        text: 'October'
      }, {
        id: 11,
        text: 'November'
      }, {
        id: 12,
        text: 'December'
      }],
      years: [ {
        id: 2016,
        text: '2016'
      }, {
        id: 2015,
        text: '2015'
      }, {
        id: 2014,
        text: '2014'
      }, {
        id: 2013,
        text: '2013'
      }, {
        id: 2012,
        text: '2012'
      }, {
        id: 2011,
        text: '2011'
      }, {
        id: 2010,
        text: '2010'
      }, {
        id: 2009,
        text: '2009'
      }, {
        id: 2008,
        text: '2008'
      }, {
        id: 2007,
        text: '2007'
      }, {
        id: 2006,
        text: '2006'
      }, {
        id: 2005,
        text: '2005'
      }, {
        id: 2004,
        text: '2004'
      }, {
        id: 2003,
        text: '2003'
      }, {
        id: 2002,
        text: '2002'
      }, {
        id: 2001,
        text: '2001'
      }, {
        id: 2000,
        text: '2000'
      }, {
        id: 1999,
        text: '1999'
      }, {
        id: 1998,
        text: '1998'
      }, {
        id: 1997,
        text: '1997'
      }, {
        id: 1996,
        text: '1996'
      }, {
        id: 1995,
        text: '1995'
      }, {
        id: 1994,
        text: '1994'
      }, {
        id: 1993,
        text: '1993'
      }, {
        id: 1992,
        text: '1992'
      }, {
        id: 1991,
        text: '1991'
      }, {
        id: 1990,
        text: '1990'
      }, {
        id: 1989,
        text: '1989'
      }, {
        id: 1988,
        text: '1988'
      }, {
        id: 1987,
        text: '1987'
      }, {
        id: 1986,
        text: '1986'
      }, {
        id: 1985,
        text: '1985'
      }, {
        id: 1984,
        text: '1984'
      }, {
        id: 1983,
        text: '1983'
      }, {
        id: 1982,
        text: '1982'
      }, {
        id: 1981,
        text: '1981'
      }, {
        id: 1980,
        text: '1980'
      }, {
        id: 1979,
        text: '1979'
      }, {
        id: 1978,
        text: '1978'
      }, {
        id: 1977,
        text: '1977'
      }, {
        id: 1976,
        text: '1976'
      }, {
        id: 1975,
        text: '1975'
      }, {
        id: 1974,
        text: '1974'
      }, {
        id: 1973,
        text: '1973'
      }, {
        id: 1972,
        text: '1972'
      }, {
        id: 1971,
        text: '1971'
      }, {
        id: 1970,
        text: '1970'
      }, {
        id: 1969,
        text: '1969'
      }, {
        id: 1968,
        text: '1968'
      }, {
        id: 1967,
        text: '1967'
      }, {
        id: 1966,
        text: '1966'
      }, {
        id: 1965,
        text: '1965'
      }, {
        id: 1964,
        text: '1964'
      }, {
        id: 1963,
        text: '1963'
      }, {
        id: 1962,
        text: '1962'
      }, {
        id: 1961,
        text: '1961'
      }, {
        id: 1960,
        text: '1960'
      }, {
        id: 1959,
        text: '1959'
      }, {
        id: 1958,
        text: '1958'
      }, {
        id: 1957,
        text: '1957'
      }, {
        id: 1956,
        text: '1956'
      }, {
        id: 1955,
        text: '1955'
      }, {
        id: 1954,
        text: '1954'
      }, {
        id: 1953,
        text: '1953'
      }, {
        id: 1952,
        text: '1952'
      }, {
        id: 1951,
        text: '1951'
      }, {
        id: 1950,
        text: '1950'
      }, {
        id: 1949,
        text: '1949'
      }, {
        id: 1948,
        text: '1948'
      }, {
        id: 1947,
        text: '1947'
      }, {
        id: 1946,
        text: '1946'
      }, {
        id: 1945,
        text: '1945'
      }, {
        id: 1944,
        text: '1944'
      }, {
        id: 1943,
        text: '1943'
      }, {
        id: 1942,
        text: '1942'
      }, {
        id: 1941,
        text: '1941'
      }, {
        id: 1940,
        text: '1940'
      }, {
        id: 1939,
        text: '1939'
      }, {
        id: 1938,
        text: '1938'
      }, {
        id: 1937,
        text: '1937'
      }, {
        id: 1936,
        text: '1936'
      }, {
        id: 1935,
        text: '1935'
      }, {
        id: 1934,
        text: '1934'
      }, {
        id: 1933,
        text: '1933'
      }, {
        id: 1932,
        text: '1932'
      }, {
        id: 1931,
        text: '1931'
      }, {
        id: 1930,
        text: '1930'
      }, {
        id: 1929,
        text: '1929'
      }, {
        id: 1928,
        text: '1928'
      }, {
        id: 1927,
        text: '1927'
      }, {
        id: 1926,
        text: '1926'
      }, {
        id: 1925,
        text: '1925'
      }, {
        id: 1924,
        text: '1924'
      }, {
        id: 1923,
        text: '1923'
      }, {
        id: 1922,
        text: '1922'
      }, {
        id: 1921,
        text: '1921'
      }, {
        id: 1920,
        text: '1920'
      }, {
        id: 1919,
        text: '1919'
      }, {
        id: 1918,
        text: '1918'
      }, {
        id: 1917,
        text: '1917'
      }, {
        id: 1916,
        text: '1916'
      }, {
        id: 1915,
        text: '1915'
      }, {
        id: 1914,
        text: '1914'
      }, {
        id: 1913,
        text: '1913'
      }, {
        id: 1912,
        text: '1912'
      }, {
        id: 1911,
        text: '1911'
      }, {
        id: 1910,
        text: '1910'
      }, {
        id: 1909,
        text: '1909'
      }, {
        id: 1908,
        text: '1908'
      }, {
        id: 1907,
        text: '1907'
      }, {
        id: 1906,
        text: '1906'
      }, {
        id: 1905,
        text: '1905'
      }, {
        id: 1904,
        text: '1904'
      }, {
        id: 1903,
        text: '1903'
      }, {
        id: 1902,
        text: '1902'
      }, {
        id: 1901,
        text: '1901'
      }, {
        id: 1900,
        text: '1900'
      }],
      incomes: [{
        id: 1,
        text: '$0-$24,999'
      }, {
        id: 2,
        text: '$25,000-$49,999'
      }, {
        id: 3,
        text: '$50,000-$74,999'
      }, {
        id: 4,
        text: '$75,000-$99,999'
      }, {
        id: 5,
        text: '$100,000-$124,999'
      }, {
        id: 6,
        text: '$125,000-$149,999 '
      }, {
        id: 7,
        text: '$150,000-$174,999'
      }, {
        id: 8,
        text: '$175,000+'
      }
      ]
    }
  },
  methods: {
    goToMain() {
      return this.$root.evaluateMarkets(-1, true)
        .then((valid) => {
          if (typeof valid === 'object') {
            valid = valid.valid;
          }

          if (valid) {
            return this.$router.go('/');
          }

          return this.$router.go(this.menulink);
        });
    },
    updateCheckbox(value, key) {
      this.contestantInfo[key] = value;
    },
    toggleOverlay: function () {
      this.showOverlay = !this.showOverlay
      window.scroll(0,0)
    },
    createContestantInfo(contestant, subscriber) {
      const contestantInfo = Object.assign({}, contestant, ...Object.entries(subscriber)
        .filter(([key, value]) => ![null, undefined, ''].includes(value) && !['dob', 'number'].includes(key))
        .map(([key, value]) => ({ [key]: value }))
      );

      if (subscriber.number) {
        contestantInfo.mobileNumber = subscriber.number;
      }

      if (subscriber.dob) {
        const dob = subscriber.dob;
        contestantInfo.dobm = dob.substring(0, 2);
        contestantInfo.dobd = dob.substring(2, 4);
        contestantInfo.doby = dob.substring(4);
      }

      if (this.contestantInfo.kids === false) {
        this.contestantInfo.kids = 2;
      } //The value feature of vf-select doesn't seem to handle 0 or false.

      return contestantInfo;
    },
    getSubscriberInfo: function(cb) {
      var subscriberGUID = Cookie.get('subscriberGUID')
      return this.$http.get(this.$config('baseURL') + '/subscribers/getByGUID?subscriberGUID=' + subscriberGUID).then((response) => {
        this.subscriber = JSON.parse(response.body).records[0]

        this.contestantInfo = this.createContestantInfo(this.contestantInfo, this.subscriber);
        if (cb != null) cb()
      })
    }
  }
}
