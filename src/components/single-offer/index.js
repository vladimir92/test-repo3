import Cookie from 'js-cookie';
import _ from 'lodash';
import moment from 'moment/moment';

export default {
  components: {
    'location-overlay': require('../location/location'),
    'barcode': require('../coupon-code')
  },
  data () {
    const shortCode = this.$appConfig('build').env.shortCode || '82928';
    const optinKeyword = this.$appConfig('build').env.optinKeyword || 'TapOnIt';
    const clientHome = this.$appConfig('build').env.clientHome || 'http://www.taponitdeals.com';
    const phone = this.$appConfig('build').env.phone || '1-844-558-6766';
    const email = this.$appConfig('build').env.email || 'taponit@taponitdeals.com';
    const offerTypeDescLcase = this.$appConfig('build').env.offerTypeDescLcase || 'offer';
    const offerTypeDescUcase = this.$appConfig('build').env.offerTypeDescUcase || 'OFFER';
    const offerTypeDescPcase = this.$appConfig('build').env.offerTypeDescPcase || 'Offer';
    return {
      tutorial: {
        tutorialNeeded: false,
        tutorialPage: 0,
        tutorialName: 'newSaveOfferTutorial',
        totalTutorialPages: 1
      },
      overlay: {
        coupons: false,
        redeem: false,
        share: false,
        menu: false,
        explanation: false,
        facebook: false,
        twitter: false,
        firstSavedOfferMessage: false,
        video: false,
        popupImage: false,
        location: false,
        popupText: false
      },
      redirectURL: window.location.origin + this.$route.path,
      shortURL: window.location.origin,
      shortURLs: [],
      shareButtonData: {
        title: 'Are you up for more?',
        description: 'Share with someone new to unlock this bonus offer!',
        isDefaultButtonLabel: true
      },
      disableRedeemButton: false,
      noCampaign: false,
      ready: false,
      newDesign: this.$root.validMarket,
      currentOffer: 0,
      subscriberGUID: '',
      offer: {},
      buttons: [],
      savedOffers: [],
      savedOfferIDs: [],
      offerSaved: false,
      currentVideoID: null,
      poupTextContent: '',
      popupImageURL: '',
      popupImageCaption: '',
      contest: false,
      offerToRedeem: null,
      errorMessage: '',
      subscriber: {},
      isPreview: false,
      shortCode,
      optinKeyword,
      clientHome,
      phone,
      email,
      smsText: '',
      offerTypeDescLcase,
      offerTypeDescUcase,
      offerTypeDescPcase
    }
  },
  computed: {
    smsHref() {
      const ua = navigator.userAgent.toLowerCase();
      let url;
      const preData = 'sms:';
      this.smsText = `Check out this deal from ${this.$root.$config('siteName')} ${this.safeURL}`
      if (/iphone|ipad/gi.test(ua)) {
        url = '&body=' + encodeURIComponent(this.smsText);
      } else if (/android/gi.test(ua)) {
        url = '?body=' + encodeURIComponent(this.smsText);
      } else {
        url = `?&body=${this.smsText}`;
      }

      return preData + url;
    },
    hasContest: function() {
      return this.find(this.contest, ['marketID', this.offer.marketID]) !== undefined
    },
    calculateClass() {
      let classes = '';
      if (this.$root.validMarket) {
        classes += 'button2'
      } else if(this.hasContest && this.offer.redeemable) {
        classes += 'square '
      }
      return classes;
    },
    safeURL: function() {
      if (this.shortURLs[this.currentOffer]) {
        if (this.shortURLs[this.currentOffer].indexOf('#') !== -1) {
          var array = this.shortURLs[this.currentOffer].split('#')
          var safeURL = encodeURI(array[0]) + '%23' + encodeURI(array[1])
        } else {
          safeURL = this.shortURLs[this.currentOffer]
        }
        return safeURL
      }
    },
    currentButtons: function() {
      return _.map(
        _.filter(this.buttons, { type: 'button', offerID: this.offer.offerID}),
        (p) => {
          if (['button_redeem', 'button_share', 'button_bonus_redeem'].indexOf(p.name) !== -1) {
            p.hidden = true;
          }
          p.fields = _.map(
            _.filter(this.buttons, { parentID: parseInt(p.offerFieldID), sort: parseInt(p.sort) }), f => {
              return f;
            });
          const existing = _.findIndex(p.fields, { type: 'button_label' });
          p.buttonLabel = p.fields[existing].value;
          p.labelID = p.fields[existing].offerFieldID;
          p.fields.splice(existing, 1);
          return p;
        }
      ).sort((a, b) => a.sort - b.sort);
    },
    expirationDate() {
      return this.offer.isBonusOffer ? this.offer.bonusOfferExpirationDate : this.offer.expirationDate;
    },
    getDateText() {
      const date = new moment(this.expirationDate);
      const [ days, hours, minutes ] = ['d', 'h', 'm'].map((e) => date.diff(moment.now(), e));

      if (minutes <= 0) {
        return `OFFER EXPIRED: ${date.format('MMMM DD, YYYY')}`;
      } else if (days > 6) {
        return `OFFER EXPIRES: ${date.format('MMMM DD, YYYY')}`;
      } else if (days > 0) {
        return `OFFER EXPIRES IN ${days} DAY${days === 1 ? '' : 'S'}`;
      } else if (hours > 0) {
        return `OFFER EXPIRES IN ${hours} HOUR${hours === 1 ? '' : 'S'}`;
      } else {
        return `OFFER EXPIRES IN ${minutes} MINUTE${minutes === 1 ? '' : 'S'}`;
      }
    },
    isExpired() {
      return new Date(this.expirationDate) < Date.now();
    }
  },
  methods: {
    logButtonClick(button, redirect = null) {
      const tag = {
        name: button.name,
        offerFieldID: button.offerFieldID,
        fields: []
      };

      if (button.fields !== undefined && button.fields.length > 0) {
        tag.fields = button.fields.map((field) => {
          return {
            name: field.name,
            offerFieldID: field.offerFieldID,
            value: field.value
          };
        });
      }

      const data = {
        page: 'SingleOffer',
        subscriberID: this.subscriber.subscriberID,
        offerID: this.offer.offerID,
        tag: JSON.stringify(tag),
        localTimezoneOffset: -(new Date().getTimezoneOffset() / 60),
        userAgent: navigator.userAgent
      };

      this.$http.post(this.$config('baseURL') + '/subscribers/click', data).then((response) => {
        if (redirect !== null) this.$route.router.go(redirect);
      }).catch(console.error);
    },
    getFieldByName(fields, name = 'button_link_url') {
      return _.find(fields, { name }).value;
    },
    getContestUrl: function(token) {
      if (token.indexOf('://') > -1) {
        return token
      } else {
        return '/c/' + token
      }
    },
    slideLeft: function() {
      if ($('.slide.show').prev('.slide').length) {
        $('.slide.show').removeClass('show').prev().addClass('show');
      } else {
        $('.slide.show').removeClass('show').next().addClass('show');
      }

    },
    slideRight: function() {
      if ($('.slide.show').next('.slide').length) {
        $('.slide.show').removeClass('show').next().addClass('show');
      } else {
        $('.slide.show').removeClass('show').prev().addClass('show');
      }

    },
    getSubscriberInfo: function(cb) {
      var subscriberGUID = Cookie.get('subscriberGUID')
      return this.$http.get(this.$config('baseURL') + '/subscribers/getByGUID?subscriberGUID=' + subscriberGUID).then((response) => {
        this.subscriber = JSON.parse(response.body).records[0]
        if (cb != null) cb()
      })
    },
    find(collection, key) {
      return _.find(collection, key)
    },

    findIndex(collection, key) {
      return _.findIndex(this[collection], [key[0], key[1]])
    },

    redeemOffer: function() {
      if (this.offerToRedeem === this.offer.offerID && !this.disableRedeemButton) {
        this.disableRedeemButton = true;
        if (!this.$root.validMarket) {
          return this.sendRequestForRedeem();
        }

        if (navigator.geolocation) {
          return navigator.geolocation.getCurrentPosition(
            (location) => this.sendRequestForRedeem(location.coords),
            () => this.sendRequestForRedeem()
          );
        } else {
          return this.sendRequestForRedeem();
        }
      }
    },

    sendRequestForRedeem(latLonObject = { longitude: null, latitude: null }) {
      const requestedOffer = this.offer;
      return this.$http.post(this.$config('baseURL') + '/offers/redeem', {
        campaignID: 0,
        offerID: requestedOffer.offerID,
        subscriberGUID: this.subscriberGUID,
        longitude: latLonObject.longitude,
        latitude: latLonObject.latitude
      }).then(() => {
        return this.$http.get(this.$config('baseURL') + '/offers/getSubscriberOffer?offerGUID=' +
          this.$route.params.offerGUID + '&subscriberGUID=' + this.subscriberGUID)
          .then((response) => response.json())
          .then((response) => {
            this.disableRedeemButton = true;
            this.closeOverlay();
            this.offer = response.records[0];
            if (this.offer.hasCoupon) {
              const requestData = {
                offerID: requestedOffer.offerID,
                subscriberGUID: this.subscriberGUID
              };

              return this.$http.post(this.$config('baseURL') + '/coupons/redeemAndGetCouponCodes', requestData)
                .then((response) => response.json())
                .then((couponResponse) => {
                  this.offer.couponData = couponResponse.records[0];
                  this.openRedeem();
                });
            }
          })
          .catch((e) => {
            this.disableRedeemButton = true;
            this.errorMessage = 'Error during refetching of offer occured. Please refresh the page.';
          })
      }, (response) => {
        // error
        this.disableRedeemButton = true;
        const obj = JSON.parse(response.body);
        if (obj.errors.error === 'second redemption') {
          this.errorMessage = 'Please wait a few seconds before redeeming ' + requestedOffer.businessName + '.';
        }
      })
    },

    openVideo: function() {
      this.currentVideoID = this.offer.youtubeVideoID;
      this.overlay.video = true;
    },

    openPopupImage: function(fields) {
      this.popupImageURL = _.find(fields, { name: 'button_popup_image_url' }).value.split('/').pop();
      this.popupImageCaption = _.find(fields, { name: 'button_popup_image_caption' }).value.split('/').pop();
      this.overlay.popupImage = true;
    },

    openPopupText: function(fields) {
      this.popupTextContent = _.find(fields, { name: 'button_popup_text_content' }).value.split('/').pop();
      this.overlay.popupText = true;
    },

    openShare(button) {
      if (this.isPreview) return;
      if (button) {
        this.shareButtonData.isDefaultButtonLabel = button.buttonLabel === 'Unlock Bonus Offer';
        button.fields.forEach((e) => {
          if (e.name === 'button_popup_text') {
            this.shareButtonData.title = e.value || e.defaultValue;
          } else if (e.name === 'button_popup_description') {
            this.shareButtonData.description = e.value || e.defaultValue;
          }
        });
      }

      if (this.shortURLs[this.currentOffer] == undefined) {
        this.$http.post(this.$config('baseURL') + '/shortURL/create', {
          "offerGUID": this.offer.offerGUID,
          "subscriberGUID": this.subscriberGUID
        }).then((response) => {
          this.shortURLs[this.currentOffer] = JSON.parse(response.body)
          this.overlay.share = true
        })
      } else {
        this.overlay.share = true
      }
    },

    explanation: function(site) {
      this.overlay.share = false
      this.overlay.explanation = true
      this.overlay[site] = true
    },

    facebook: function() {
      var self = this

      let href = this.shortURL + '/o/' + this.offer.offerGUID + '/' + this.subscriberGUID;
      if (this.$route.params.campaignID && parseInt(this.$route.params.campaignID) > 0) {
        href += '/' + this.$route.params.campaignID;
      }
      console.info('single offer facebook', href);

      FB.ui({
        method: 'feed',
        name: this.offer.businessName,
        link: href,
        caption: `Click here and sign up to get this great deal and more from ${this.$root.$config('siteName')}!`
      },
        function(response) {
          console.info('single offer facebook response', response);
          if (response && !response.error_message) {
            self.$http.post(self.$config('baseURL') + '/offers/createShare', {
              "offerID": self.offer.offerID,
              "campaignID": 0,
              "subscriberGUID": self.subscriberGUID,
              "sharedTo": "Facebook"
            })
          } else {
          }
        });
    },

    twitter: function() {
      this.$http.post(this.$config('baseURL') + '/offers/createShare', {
        "offerID": this.offer.offerID, "campaignID": 0,
        "subscriberGUID": this.subscriberGUID,
        "sharedTo": "Twitter"
      }).then((result) => {
        window.location.href = 'https://twitter.com/intent/tweet?text=Check out this great deal from ' + this.offer.businessName + '!&url=' + this.safeURL
      })
    },

    copyToClipboard() {
      const el = document.createElement('textarea');
      el.value = this.smsText;
      document.body.appendChild(el);
      el.setAttribute('readonly', '');
      el.style.position = 'absolute';
      el.style.left = '-9999px';
      el.select();
      document.execCommand('copy');
      document.body.removeChild(el);
    },
    sms: function() {
      this.copyToClipboard();
      this.$http.post(this.$config('baseURL') + '/offers/createShare', {
        "offerID": this.offer.offerID,
        "campaignID": this.$route.params.campaignID,
        "subscriberGUID": this.subscriberGUID,
        "sharedTo": "SMS"
      }).then((result) => {
      })
    },

    openRedeem() {
      if (this.isPreview) return;
      if (this.offer.redeemed && this.offer.hasCoupon) {
        this.overlay.coupons = true;
      } else {
        this.overlay.redeem = true;
        this.offerToRedeem = this.offer.offerID;
      }
    },

    openLocation() {
      this.overlay.location = true;
    },

    openMenu: function() {
      this.overlay.menu = true
    },

    closeOverlay: function() {
      this.overlay.share = false
      this.overlay.redeem = false
      this.overlay.menu = false
      this.overlay.explanation = false
      this.overlay.facebook = false
      this.overlay.twitter = false
      this.overlay.location = false;
      this.overlay.firstSavedOfferMessage = false
      this.overlay.video = false;
      this.currentVideoID = null;
    },

    saveOffer: function() {
      if (this.isPreview) return;
      this.offer.campaignID = this.offer.campaignID || 0;
      this.savedOffers.push({offerID: this.offer.offerID.toString(), marketID: this.offer.marketID.toString()})
      this.offer.saveOfferID = true
      Cookie.set('savedOffersMarkets', JSON.stringify(this.savedOffers), { expires: 365 })
      this.$http.post(this.$config('baseURL') + '/subscribers/saveOffer', {
        offerGUID: this.offer.offerGUID,
        subscriberGUID: this.subscriberGUID,
        campaignID: this.offer.campaignID
      }).then((response) => {
        this.offerSaved = true;
        this.savedOfferIDs.push(this.offer.offerID);
        if (!this.subscriber.hasSavedOffers) {
          this.subscriber.hasSavedOffers = true
          this.overlay.firstSavedOfferMessage = true
        }
      });
    },
    confirmRemoveSavedOffer() {
      this.$http.delete(
        this.$config('baseURL') + '/subscribers/removeOfferSave?offerGUID=' + this.offer.offerGUID +
          '&subscriberGUID=' + this.subscriberGUID + '&campaignID=' + this.offer.campaignID || 0
      ).then(() => {
        this.offerSaved = false;
        this.savedOfferIDs.splice(this.savedOfferIDs.indexOf((e) => e === this.offer.offerID), 1);
      });
    },
    loadButtons: function() {
      this.$http.get(`${this.$config('baseURL')}/offerField/getAllAttached?offerID=${this.offer.offerID}`).then(response => {
        this.buttons = JSON.parse(response.body).records;
        this.buttons.slice(0);
      }).catch(e => {
        console.error(e);
        debugger;
      });
    },
    isTutorialNeeded() {
      const query = `subscriberGUID=${this.subscriberGUID}&tutorial=${this.tutorial.tutorialName}`;
      return this.$http.get(this.$config('baseURL') + '/subscribers/checkTutorial?' + query)
        .then((r) => r.json())
        .then((response) => {
          this.tutorial.tutorialPage = 0;
          this.tutorial.tutorialNeeded = !response.metadata.inactive && response.records.length === 0;
        });
    },
    finishedTutorial() {
      this.tutorial.tutorialNeeded = false;
      const requestBody = {
        subscriberGUID: this.subscriberGUID,
        tutorial: this.tutorial.tutorialName
      };
      return this.$http.post(this.$config('baseURL') + '/subscribers/finishTutorial', requestBody);
    },
    nextPage() {
      this.tutorial.tutorialPage++;
      if (this.tutorial.totalTutorialPages === this.tutorial.tutorialPage) {
        this.finishedTutorial();
      }
    }
  },
  route: {
    canReuse: false,
    data: function() {
      // set subscriber GUID
      const oldSubscriber = Cookie.get('subscriberGUID');
      if (this.$route.params.subscriberGUID !== undefined && this.$route.params.subscriberGUID !== 'undefined') {
        this.subscriberGUID = this.$route.params.subscriberGUID
        Cookie.set('subscriberGUID', this.subscriberGUID, { expires: 365 })
      } else if (Cookie.get('subscriberGUID') !== undefined) {
        this.subscriberGUID = Cookie.get('subscriberGUID')
      }
      if (oldSubscriber !== this.subscriberGUID) {
        this.$root.evaluateMarkets().then((valid) => {
          this.$children.find(e => e.$options.name === 'joi-header').newDesign = valid;
          this.newDesign = valid;
        });
      }

      this.getSubscriberInfo()
      this.isTutorialNeeded();

      // get or set saved offers cookie
      // clear saved offers for switch to different syntax
      if (Cookie.get('savedOffersMarkets') === undefined) {
        Cookie.set('savedOffersMarkets', '[]', { expires: 365 })
      }

      // get saved offers
      this.$http.get(this.$config('baseURL') + '/subscribers/getSavedOffers?subscriberGUID=' + this.subscriberGUID + '&cache=' + Date.now()).then((response) => {
        this.savedOfferIDs = JSON.parse(response.body).records.map(offer => offer.offerID)
        this.offerSaved = this.savedOfferIDs.some((e) => e === this.offer.offerID);
      }, (response) => {
        // error
      })

      this.savedOffers = JSON.parse(Cookie.get('savedOffersMarkets'))

      // grab offer
      this.$http.get(this.$config('baseURL') + '/offers/getSubscriberOffer?offerGUID=' + this.$route.params.offerGUID + '&subscriberGUID=' + this.subscriberGUID).then((response) => {

        this.offer = JSON.parse(response.body).records[0]

        if (this.offer.hasCoupon) {
          const requestURL = [
            this.$config('baseURL'),
            '/coupons/getBySubscriberGUIDAndOfferID?',
            `offerID=${this.offer.offerID}`,
            `&subscriberGUID=${this.subscriberGUID}`,
            '&appendCount=1'
          ].join('');

          this.$http.get(requestURL)
            .then((r) => r.json())
            .then((couponResponse) => {
              this.offer.couponData = couponResponse.records[0];
            });
        }

        this.offerSaved = this.savedOfferIDs.some((e) => e === this.offer.offerID);

        this.loadButtons();
        this.$http.get(this.$config('baseURL') + '/contest/getByMarketID?marketID=' + this.offer.marketID).then(response => {
          if (JSON.parse(response.body).metadata.rowcount > 0)
            this.contest = JSON.parse(response.body).records.map(contest => {
              if (moment().toISOString() <= contest.endDate && moment().toISOString() >= contest.startDate) {
                return ({ "marketID": contest.marketID, "urlToken": contest.urlToken, "buttonText": contest.buttonText })
              }
            })
          else {
            this.contest = false
          }
        }, response => {
          // error
        })

        if (this.offer !== undefined) {
          this.noCampaign = false
          this.ready = true
        }

        // store referrer
        let referrer = Cookie.get('referrer')

        this.$http.post(this.$config('baseURL') + '/metrics/offerClick', { "url": window.location.href, "referrer": referrer || window.document.referrer, "marketID": this.offer.marketID, "offerID": this.offer.offerID, "subscriberGUID": this.subscriberGUID }).then((response) => {
          Cookie.remove('referrer')
        })
      }, (response) => {
        this.noCampaign = true
      })
    }
  }
}
