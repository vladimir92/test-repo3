import _ from 'lodash';
import Cookie from 'js-cookie';

import formsMixin from '../../mixins/forms.js';

export default {
  mixins: [formsMixin],
  data() {
    const shortCode = this.$appConfig('build').env.shortCode || '82928';
    const optinKeyword = this.$appConfig('build').env.optinKeyword || 'TapOnIt';
    const clientHome = this.$appConfig('build').env.clientHome || 'http://www.taponitdeals.com';
    const phone = this.$appConfig('build').env.phone || '1-844-558-6766';
    const email = this.$appConfig('build').env.email || 'taponit@taponitdeals.com';
    const offerTypeDescLcase = this.$appConfig('build').env.offerTypeDescLcase || 'offer';
    const offerTypeDescUcase = this.$appConfig('build').env.offerTypeDescUcase || 'OFFER';
    const offerTypeDescPcase = this.$appConfig('build').env.offerTypeDescPcase || 'Offer';
    return {
      formOptions: {
        name: 'profile',
        resource: 'subscribers',
        httpAction: 'put'
      },
      form: {
        zipCode: '',
        subscriberGUID: ''
      },
      overlay: {
        redeem: false,
        share: false,
        menu: false,
        explanation: false,
        facebook: false,
        twitter: false,
        noZip: false,
        firstSavedOfferMessage: false,
        video: false,
        popupImage: false,
        popupText: false
      },
      buttons: [],
      subscriberGUID: '',
      redirectURL: 'http://' + window.location.host + this.$route.path,
      shortURL: 'http://' + window.location.host,
      shortURLs: [],
      redeemed: [],
      noCampaign: null,
      noSubscriber: false,
      currentOffer: 0,
      currentTranslate: 0,
      transitioning: false,
      offers: [],
      contest: [],
      savedOffers: [],
      savedOfferIDs: [],
      currentVideoID: null,
      poupTextContent: '',
      popupImageURL: '',
      popupImageCaption: '',
      requestingZip: false,
      offerToRedeem: null,
      errorMessage: '',
      subscriber: {},
      market: { city: '', state: '', zipCode: '' },
      isPreview: false,
      shortCode,
      optinKeyword,
      clientHome,
      phone,
      email,
      offerTypeDescLcase,
      offerTypeDescUcase,
      offerTypeDescPcase,
      showMarketName: false
    };
  },
  computed: {
    hasContest: function () {
      const contestExist = this.find(this.offers[this.currentOffer].contest,
        ['marketID', this.offers[this.currentOffer].marketID]
      );
      return contestExist ? 1 : 0;
    },
    safeURL: function () {
      let safeURL = '';
      if (this.shortURLs[this.currentOffer]) {
        if (this.shortURLs[this.currentOffer].indexOf('#') !== -1) {
          let array = this.shortURLs[this.currentOffer].split('#');
          safeURL = encodeURI(array[0]) + '%23' + encodeURI(array[1]);
        } else {
          safeURL = this.shortURLs[this.currentOffer];
        }
        return safeURL;
      }
    },
    currentOfferID: function () {
      return this.offers[this.currentOffer] && this.offers[this.currentOffer].offerID;
    },
    marketName: function () {
      // return this.market.city !== '' ? `${this.market.city}, ${this.market.state}` : '';
      return this.market.state ? this.market.city + ', ' + this.market.state : this.market.city;
    },
    showMarket: function () {
      return this.market.state ? this.market.city + ', ' + this.market.state : this.market.city;
    },
    currentButtons: function () {
      const buttonsToSort = this.buttons.filter(button => button.offerID === this.currentOfferID);

      const sortedButtons = _.map(
        _.filter(buttonsToSort, { type: 'button', offerID: this.currentOfferID }),
        (p) => {
          if (['button_redeem', 'button_share', 'button_bonus_redeem'].indexOf(p.name) !== -1) {
            p.hidden = true;
          }
          p.fields = _.map(
            _.filter(buttonsToSort, { parentID: parseInt(p.offerFieldID), sort: parseInt(p.sort) }), f => {
              return f;
            });
          const existing = _.findIndex(p.fields, { type: 'button_label' });
          p.buttonLabel = p.fields[existing].value;
          p.labelID = p.fields[existing].offerFieldID;
          p.fields.splice(existing, 1);
          return p;
        }
      ).sort((a, b) => a.sort > b.sort);
      console.info(sortedButtons);
      return sortedButtons;
    }
  },
  ready: function () {
    this.getSubscriberInfo(() => {
      if (!this.doesUserHaveZipCodeFilledOut()) {
        this.overlay.noZip = true;
      }
    });

    this.$on('closeOverlay', function () {
      if (this.requestingZip) {
        this.requestingZip = false;
        this.getSubscriberInfo(() => {
          if (!this.doesUserHaveZipCodeFilledOut()) {
            this.overlay.noZip = true;
          }
        });
      }
    });
  },
  methods: {
    getLinkUrl: function (fields) {
      return _.find(fields, { name: 'button_link_url' }).value;
    },
    getContestUrl: function (token) {
      if (token.indexOf('://') > -1) {
        return token;
      } else {
        return '/c/' + token;
      }
    },
    getSubscriberInfo: function (cb) {
      return this.$http.get(this.$config('baseURL') +
        '/subscribers/getByGUID?subscriberGUID=' + this.subscriberGUID)
        .then((response) => {
          this.subscriber = JSON.parse(response.body).records[0];
          if (cb != null) cb();
        });
    },
    doesUserHaveZipCodeFilledOut: function () {
      let zip = this.subscriber && this.subscriber.zipCode ? this.subscriber.zipCode.trim() : null;
      return zip !== null && zip !== '';
    },

    contestCheck: function (marketID) {
      return _.find(this.contest, { marketID });
    },

    find(collection, key) {
      return _.find(collection, key);
    },

    findIndex(collection, key) {
      return _.findIndex(this[collection], [key[0], key[1]]);
    },
    transitionEnd: function () {
      this.transitioning = false;
    },

    submitZip: function (e) {
      this.closeOverlay();
      this.submitForm(this.formOptions, e);
    },

    showProfile: function () {
      this.requestingZip = true;
      this.overlay.noZip = false;
      this.$broadcast('showProfile');
    },

    slideLeft() {
      let offerImage = document.getElementById('offer-images');
      let offerWidth = offerImage.offsetWidth < 500 ? offerImage.offsetWidth : 500;
      this.transitioning = true;
      if (this.currentOffer === 0) {
        offerImage.style.transform = 'translateX(' + -((+this.offers.length - 1) * offerWidth) + 'px)';
        this.currentTranslate = -((+this.currentOffer + 1) * 100);
        this.currentOffer = this.offers.length - 1;
      } else {
        offerImage.style.transform = 'translateX(' + -((+this.currentOffer - 1) * offerWidth) + 'px)';
        this.currentTranslate = -((+this.currentOffer - 1) * 100);
        this.currentOffer--;
      }
      // this.updateButtonCount();
    },
    slideRight: function () {
      let offerImage = document.getElementById('offer-images');
      let offerWidth = offerImage.offsetWidth < 500 ? offerImage.offsetWidth : 500;
      this.transitioning = true;
      if (this.currentOffer === this.offers.length - 1) {
        offerImage.style.transform = 'translateX(' + -((+0) * offerWidth) + 'px)';
        this.currentOffer = 0;
        this.currentTranslate = -((this.currentOffer + 1) * offerWidth);
      } else {
        offerImage.style.transform = 'translateX(' + -((+this.currentOffer + 1) * offerWidth) + 'px)';
        this.currentTranslate = -((+this.currentOffer + 1) * 100);
        this.currentOffer++;
      }
      // this.updateButtonCount();
    },
    onPan: function (e) {
      if (this.transitioning) {
        return;
      }

      let offerImage = document.getElementById('offer-images');
      let offerImageWidth = document.getElementsByClassName('offer-image')[0].offsetWidth;
      let offerImagePercentage = (Math.ceil((e.deltaX / offerImageWidth) * 100));
      let offerImagePercentageAdjusted = +offerImagePercentage + +this.currentTranslate;

      if (e.isFinal) {
        console.info(e.target.id,
          `${offerImagePercentage}%${offerImagePercentageAdjusted}%${this.currentTranslate}`
        );
      }
      if (e.target.id === '0' && e.deltaX > 0) {
        offerImage.style.transform = 'translateX(' + -((+this.offers.length - 1) * offerImageWidth) + 'px)';
        this.currentOffer = this.offers.length - 1;
        this.currentTranslate = -((+this.currentOffer) * 100);
      } else if (e.target.id === this.offers.length - 1 && e.deltaX < 0) {
        // at the end, skip to front
        offerImage.style.transform = 'translateX(' + -((+0) * offerImageWidth) + 'px)';
        this.currentOffer = 0;
        this.currentTranslate = 0;
      } else {
        offerImage.classList.remove('active');
        offerImage.style.transform = 'translate3D(' + offerImagePercentageAdjusted + '%, 0, 0)';
        if (e.isFinal) {
          this.transitioning = true;
          offerImage.classList.add('active');
        }

        if (e.deltaX > -100 && e.deltaX < 100 && e.isFinal) {
          // swipe too small
          offerImage.style.transform = 'translateX(' + +this.currentTranslate + '%)';
        }

        if (e.deltaX <= -100 && e.isFinal) {
          // left swipe
          offerImage.style.transform = 'translateX(' + -((+e.target.id + 1) * offerImageWidth) + 'px)';
          this.currentTranslate = -((+e.target.id + 1) * 100);
          this.currentOffer++;
        } else if (e.deltaX >= 100 && e.isFinal) {
          // right swipe
          offerImage.style.transform = 'translateX(' + -((+e.target.id - 1) * offerImageWidth) + 'px)';
          this.currentTranslate = -((+e.target.id - 1) * 100);
          this.currentOffer--;
        }
      }
      console.info(this.currentTranslate);
      // this.updateButtonCount();
    },

    redeemOffer: function () {
      if (this.isPreview) return;
      let requestedOffer = this.offers[this.currentOffer];
      if (this.offerToRedeem === requestedOffer.offerID) {
        this.$http.post(this.$config('baseURL') + '/offers/redeem', {
          'campaignID': this.$route.params.campaignID,
          'offerID': requestedOffer.offerID,
          'subscriberGUID': this.subscriberGUID
        }).then(() => {
          this.$http.get(this.$config('baseURL') +
            '/campaigns/getOffersWithRedeemed?showCurrentOnly=1&campaignOffersOnly=1' +
            '&campaignID=' + this.$route.params.campaignID +
            '&subscriberGUID=' + this.subscriberGUID +
            '&marketID=' + this.$route.params.marketID
          ).then((response) => {
            this.offers = JSON.parse(response.body).records;
            // this.updateButtonCount();
          });
        }, (response) => {
          // error
          let obj = JSON.parse(response.body);
          if (obj.errors.error === 'second redemption') {
            this.errorMessage = 'Please wait a few seconds before redeeming ' + requestedOffer.businessName + '.';
          }
        });
      }
    },

    saveOffer: function () {
      if (this.isPreview) return;
      this.savedOffers.push({
        offerID: this.offers[this.currentOffer].offerID.toString(),
        marketID: this.offers[this.currentOffer].marketID.toString()
      });

      this.savedOfferIDs.push(this.offers[this.currentOffer].offerID);
      // this.updateButtonCount()
      Cookie.set('savedOffersMarkets', JSON.stringify(this.savedOffers), { expires: 365 });
      this.$http.post(this.$config('baseURL') + '/subscribers/saveOffer', {
        'offerGUID': this.offers[this.currentOffer].offerGUID,
        'subscriberGUID': this.subscriberGUID,
        'campaignID': this.$route.params.campaignID
      }).then(() => {
        if (!this.subscriber.hasSavedOffers) {
          this.subscriber.hasSavedOffers = true;
          this.overlay.firstSavedOfferMessage = true;
        }
      });
    },

    openVideo: function (fields) {
      this.currentVideoID = _.find(fields, {
        name: 'button_video_youtube_id'
      }).value.split('/').pop().split('=').pop();
      console.info('opening video', this.currentVideoID);
      this.overlay.video = true;
    },

    openPopupImage: function (fields) {
      this.popupImageURL = _.find(fields, { name: 'button_popup_image_url' }).value.split('/').pop();
      this.popupImageCaption = _.find(fields, { name: 'button_popup_image_caption' }).value.split('/').pop();
      this.overlay.popupImage = true;
    },

    openPopupText: function (fields) {
      this.popupTextContent = _.find(fields, { name: 'button_popup_text_content' }).value.split('/').pop();
      this.overlay.popupText = true;
    },

    openShare: function () {
      if (this.isPreview) return;
      if (this.shortURLs[this.currentOffer] === undefined) {
        this.$http.post(this.$config('baseURL') + '/shortURL/create',
          { 'offerGUID': this.offers[this.currentOffer].offerGUID, 'subscriberGUID': this.subscriberGUID })
          .then((response) => {
            console.log(this.offers[this.currentOffer].offerGUID, JSON.parse(response.body));
            this.shortURLs[this.currentOffer] = JSON.parse(response.body);
            this.overlay.share = true;
          });
      } else {
        this.overlay.share = true;
      }
    },

    explanation: function (site) {
      this.overlay.share = false;
      this.overlay.explanation = true;
      this.overlay[site] = true;
    },

    facebook: function () {
      let self = this;

      let href = this.shortURL + '/o/' + this.offers[this.currentOffer].offerGUID + '/' + this.subscriberGUID;
      if (this.$route.params.campaignID && parseInt(this.$route.params.campaignID) > 0) {
        href += '/' + this.$route.params.campaignID;
      }
      console.info('campaign facebook link', href);
      window.FB.ui({
        method: 'feed',
        name: this.offers[this.currentOffer].businessName,
        link: href,
        caption: `Click here and sign up to get this great deal and more from ${this.$root.$config('siteName')}!`
      }, (response) => {
        console.info('campaign facebook response', response);
        if (response && !response.error_message) {
          self.$http.post(self.$config('baseURL') + '/offers/createShare', {
            'offerID': self.offers[self.currentOffer].offerID,
            'campaignID': self.$route.params.campaignID,
            'subscriberGUID': self.subscriberGUID,
            'sharedTo': 'Facebook'
          });
        } else {
        }
      });
    },

    twitter: function () {
      this.$http.post(this.$config('baseURL') + '/offers/createShare', {
        'offerID': this.offers[this.currentOffer].offerID,
        'campaignID': this.$route.params.campaignID,
        'subscriberGUID': this.subscriberGUID,
        'sharedTo': 'Twitter'
      }).then((result) => {
        console.info('campaign twitter response', result);
        window.location.href = 'https://twitter.com/intent/tweet?text=Check out this great deal from ' +
          this.offers[this.currentOffer].businessName + '!&url=' + this.safeURL;
      });
    },

    sms: function () {
      this.$http.post(this.$config('baseURL') + '/offers/createShare', {
        'offerID': this.offers[this.currentOffer].offerID,
        'campaignID': this.$route.params.campaignID,
        'subscriberGUID': this.subscriberGUID,
        'sharedTo': 'SMS'
      }).then((result) => {
        console.info('campaign sms response', result);
      });
    },

    openRedeem: function () {
      if (this.isPreview) return;
      this.overlay.redeem = true;
      this.offerToRedeem = this.offers[this.currentOffer].offerID;
    },

    openMenu: function () {
      this.overlay.menu = true;
    },

    closeOverlay: function () {
      this.overlay.share = false;
      this.overlay.redeem = false;
      this.overlay.menu = false;
      this.overlay.explanation = false;
      this.overlay.facebook = false;
      this.overlay.twitter = false;
      this.overlay.firstSavedOfferMessage = false;
      this.overlay.noZip = false;
      this.overlay.video = false;
      this.overlay.popupText = false;
      this.overlay.popupImage = false;

      this.popupTextContent = '';
      this.popupImageURL = '';
      this.popupImageCaption = '';
      this.currentVideoID = null;
    },
    loadButtons: function () {
      const buttons = [];
      this.offers.forEach(o => {
        buttons.push(this.$http.get(`${this.$config('baseURL')}/offerField/getAllAttached?offerID=${o.offerID}`));
      });
      Promise.all(buttons).then(responses => {
        this.buttons = [];
        responses.forEach(r => {
          this.buttons = this.buttons.concat(JSON.parse(r.body).records);
        });
        this.buttons.slice(0);
      }).catch(e => {
        console.error(e);
      });
    },
    loadCampaignPreview: function (urlCode) {
      const previewURL = `${this.$config('baseURL')}/campaigns/getPreview?urlCode=${urlCode}`;
      this.$http.get(previewURL).then((response) => {
        if (!response) {
          return;
        }
        const results = JSON.parse(response.body);
        const offers = results && results.offers ? JSON.parse(results.offers) : [];
        this.subscriberGUID = results.subscriberGUID;
        Cookie.set('subscriberGUID', this.subscriberGUID, { expires: 365 });
        this.offers = offers;
        this.loadButtons();
      }, (response) => {
        console.error(response);
        // error
      });
    }
  },
  route: {
    canReuse: false,
    data: function () {
      if (this.$route.params.urlCode) {
        this.isPreview = true;
        return this.loadCampaignPreview(this.$route.params.urlCode);
      }

      // no subscriber
      if (Cookie.get('subscriberGUID') === undefined && this.$route.params.subscriberGUID === undefined) {
        this.noSubscriber = true;
        return;
      }

      // set subscriber GUID
      if (this.$route.params.subscriberGUID !== undefined) {
        this.subscriberGUID = this.$route.params.subscriberGUID;
      } else if (Cookie.get('subscriberGUID') !== undefined) {
        this.subscriberGUID = Cookie.get('subscriberGUID');
      }
      this.form.subscriberGUID = this.subscriberGUID;

      // get or set saved offers cookie
      // clear saved offers for switch to different syntax
      if (Cookie.get('savedOffersMarkets') === undefined) {
        Cookie.set('savedOffersMarkets', '[]', { expires: 365 });
      }

      let shortURLQuery;
      if (this.$route.query.shortURL) {
        shortURLQuery = '&shortURL=' + this.$route.query.shortURL;
      } else {
        shortURLQuery = '';
      }

      // get saved offers
      const getSavedOffersURL = `${this.$config('baseURL')}/subscribers/getSavedOffers?` +
        `subscriberGUID=${this.subscriberGUID}&cache=${Date.now()}`;
      this.$http.get(getSavedOffersURL).then((response) => {
        this.savedOfferIDs = JSON.parse(response.body).records.map(offer => offer.offerID);
      }, () => {
        // error
      });

      this.savedOffers = JSON.parse(Cookie.get('savedOffersMarkets'));

      this.$route.params.campaignID !== '0' && Cookie.set('lastCampaignID', this.$route.params.campaignID);
      this.$route.params.marketID !== undefined && Cookie.set('lastMarketID', this.$route.params.marketID);
      // const campaignID = Cookie.get('lastCampaignID') || null;
      const campaignID = this.$route.params.campaignID;
      const marketID = Cookie.get('lastMarketID') || null;

      // grab campaign  and market
      const getCampaignURL = campaignID
        ? `${this.$config('baseURL')}/campaigns/getCampaignAndMarketByID?id=${campaignID}`
        : null;
      if (getCampaignURL) {
        this.$http.get(getCampaignURL).then((response) => {
          if (JSON.parse(response.body).metadata.rowcount > 0) {
            const results = JSON.parse(response.body);
            if (results.records.length > 0) {
              const { marketID, city, state } = results.records;
              Cookie.set('lastMarketID', marketID);
              this.market = {
                marketID, city, state
              };
            }
          }
        }, () => {
          // error
        });
      }

      // if subscriber has >1 market, display the current city & state

      const getSubscribedMarketsURL = this.$config('baseURL') +
        '/subscribers/getAssignedMarkets?subscriberGUID=' + this.subscriberGUID;
      this.$http.get(getSubscribedMarketsURL).then((response) => {
        this.showMarketName = false;
        if (JSON.parse(response.body).metadata.rowcount > 1) {
          // ai temporary    this.showMarketName=true;
          // ai temporary if (this.market.city === ''){this.showMarketName=false};
        }
      }, () => {
        // error
      });

      if ((campaignID || this.$route.params.any) && this.subscriberGUID) {
        let getOffersURL = this.$config('baseURL') +
          '/campaigns/getOffersWithRedeemed?showCurrentOnly=1&campaignOffersOnly=1' +
          '&campaignID=' + campaignID +
          '&subscriberGUID=' + this.subscriberGUID + shortURLQuery;
        if (this.$route.path === '/more-offers') {
          getOffersURL = this.$config('baseURL') + '/offers/getMoreOffers?subscriberGUID=' + this.subscriberGUID +
            '&marketID=6,7,8,9,10';
        } else if (marketID) {
          getOffersURL = this.$config('baseURL') + '/campaigns/getOffersWithRedeemed?' +
          'showCurrentOnly=1&campaignOffersOnly=1' +
          '&campaignID=' + campaignID +
            '&subscriberGUID=' + this.subscriberGUID + '&marketID=' + marketID + shortURLQuery;
        }

        this.$http.get(getOffersURL).then((response) => {
          this.offers = JSON.parse(response.body).records.slice(0, 9);

          if (this.offers.length === 0) {
            this.noCampaign = true;
            throw 'No campaign.';
          } else if (this.subscriberGUID) {
            Cookie.set('subscriberGUID', this.subscriberGUID, { expires: 365 });
            this.loadButtons();

            // store referrer
            const referrer = Cookie.get('referrer');
            const offerClickURL = this.$config('baseURL') + '/metrics/offerClick';
            const offerClickParams = {
              url: window.location.href,
              referrer: referrer || window.document.referrer,
              marketID: this.$route.params.marketID,
              campaignID: this.$route.params.campaignID,
              subscriberGUID: this.subscriberGUID
            };

            this.$http.post(offerClickURL, offerClickParams).then(() => {
              Cookie.remove('referrer');
            });

            // see if contest
            return this.$http.get(this.$config('baseURL') + '/contest/search?active=1');
          }
        }, () => {
          this.noCampaign = true;
        }).then((response) => {
          if (JSON.parse(response.body).metadata.rowcount > 0) {
            this.contest = JSON.parse(response.body).records.map(contest => {
              if (new Date().toISOString() < contest.endDate) {
                return {
                  marketID: contest.marketID,
                  urlToken: contest.urlToken,
                  buttonText: contest.buttonText
                };
              }
            });
          } else {
            this.contest = [];
          }

          // this.updateButtonCount();
        });
      } else {
        this.noCampaign = true;
      }
    }
  }
};
