﻿module.exports = {
        sortBy: function (a,f){
                for (var i=a.length;i;){
                var o = a[--i];
                a[i] = [].concat(f.call(o,o,i),o);
                }
                a.sort(function(a,b){
                for (var i=0,len=a.length;i<len;++i){
                        if (a[i]!=b[i]) return a[i]<b[i]?-1:1;
                }
                return 0;
                });
                for (var i=a.length;i;){
                a[--i]=a[i][a[i].length-1];
                }
                return a;
    },
    sortByDesc: function (a, f) {
        for (var i = a.length; i;) {
            var o = a[--i];
            a[i] = [].concat(f.call(o, o, i), o);
        }
        a.sort(function (a, b) {
            for (var i = 0, len = a.length; i < len; ++i) {
                if (a[i] != b[i]) return a[i] > b[i] ? -1 : 1;
            }
            return 0;
        });
        for (var i = a.length; i;) {
            a[--i] = a[i][a[i].length - 1];
        }
        return a;
    }
}
