const path = require('path')
const clientVersion = require('../package.json').version;
const devFile = process.env.NODE_ENV === 'local' ? require('./local') : require(`./${process.env.NODE_ENV}`);
const prodFile = process.env.NODE_ENV === 'production' ? require('./production') : require(`./${process.env.NODE_ENV}`);


module.exports = {
  build: {
    env: prodFile.env,
    baseURL: require('./production').baseURL,
    copyright: require('./production').copyright,
    facebookApiID: require('./production').facebookApiID,
    clientVersion,
    index: path.resolve(__dirname, '../dist/index.html'),
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    productionSourceMap: true,
    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css']
  },
  dev: {
    env: devFile.env,
    baseURL: devFile.baseURL,
    facebookApiID: devFile.facebookApiID,
    copyright: devFile.copyright,
    port: devFile.port || 8080,
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {},
    // CSS Sourcemaps off by default because relative paths are "buggy"
    // with this option, according to the CSS-Loader README
    // (https://github.com/webpack/css-loader#sourcemaps)
    // In our experience, they generally work as expected,
    // just be aware of this issue when enabling this option.
    cssSourceMap: false
  }
}
