let prodConfig = {
  env: {
    NODE_ENV: '"rustBelt"',
    baseURL: '"http://mobileserver-multitenancy.uh3pab4msd.us-east-1.elasticbeanstalk.com"',
    facebookApiID: '1918642011734785',
    siteName: 'Rust Belt',
    shortCode: '82928',
    optinKeyword: 'RustBelt',
    clientHome: 'https://therustbeltqc.com/',
    phone: '1-844-558-6766',
    copyright: "'Copyright © 2019, RustBelt'",
    email: 'rustbeltil@gmail.com',
    menuContent: require('../src/assets/themes/rustBelt/data/menu.json'),
    tutorialText: require('../src/assets/themes/rustBelt/data/tutorial.json')
  }
}


module.exports = prodConfig;
