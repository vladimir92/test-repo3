let prodConfig = {
  env: {
    NODE_ENV: '"production"',
    baseURL: '"https://mobileserver.tap82928.com"',
	facebookApiID: '1918642011734785',
    siteName: 'TapOnIt',
    shortCode: '82928',
    optinKeyword: 'TapOnIt',
    clientHome: 'http://www.taponitdeals.com',
    phone: '1-844-558-6766',
    copyright: "'Copyright © 2019, TapOnIt'",
    email: 'taponit@taponitdeals.com'
  }
}

module.exports = prodConfig;
