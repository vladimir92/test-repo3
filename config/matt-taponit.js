var merge = require('webpack-merge')
var prodEnv = require('./production')

module.exports = merge(prodEnv, {
  env: {
    NODE_ENV: '"matt-taponit"',
    baseURL: '"http://192.168.100.142:8879"',
	facebookApiID: '1918642011734785',
    siteName: 'TapOnIt',
    shortCode: '82928',
    optinKeyword: 'TapOnIt',
    clientHome: 'http://www.taponitdeals.com',
    phone: '1-844-558-6766',
    copyright: "'Copyright © 2018, TapOnIt'",
    email: 'taponit@taponitdeals.com'
  },
  port: 3001
})
