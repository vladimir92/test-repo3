let prodConfig = {
  env: {
    NODE_ENV: '"mac-dev"',
    baseURL: '"http://mobileserver-mac-dev.mzdymwnxwe.us-east-1.elasticbeanstalk.com"',
    facebookApiID: '2023071674634753',
    siteName: 'MAC Cosmetics',
    clientHome: 'http://www.maccosmetics.com/',
    copyright: "'Powered by TOItech™'",
    menuContent: require('../src/assets/themes/mac/data/menu.json')
  }
}

module.exports = prodConfig;
