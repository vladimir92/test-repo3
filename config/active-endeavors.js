let prodConfig = {
  env: {
    NODE_ENV: '"active-endeavors"',
    baseURL: '"http://mobileserver-multitenancy.uh3pab4msd.us-east-1.elasticbeanstalk.com"',
    facebookApiID: '1918642011734785',
    siteName: 'Active Endeavors',
    shortCode: '82928',
    optinKeyword: 'ActiveEndeavors',
    clientHome: 'https://www.activedsm.com/',
    phone: '1-844-558-6766',
    copyright: "'Copyright © 2019, ActiveEndeavors'",
    email: 'info@activedsm.com',
    menuContent: require('../src/assets/themes/activeEndeavors/data/menu.json'),
    socialMedia1: 'https://www.facebook.com/ActiveDSM',
    socialMedia2: 'https://www.instagram.com/activedsm/'
  }
}


module.exports = prodConfig;
