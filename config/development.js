let prodConfig = {
  env: {
    NODE_ENV: '"development"',
    baseURL: '"http://mobileserver-development.mzdymwnxwe.us-east-1.elasticbeanstalk.com"',
    facebookApiID: '1918642011734785',
    siteName: 'TapOnIt',
    copyright: "'Copyright © 2019, TapOnIt'"
  }
}

module.exports = prodConfig;
