var merge = require('webpack-merge');
var prodEnv = require('./production');

module.exports = merge(prodEnv, {
  env: {
    NODE_ENV: '"local"',
    baseURL: '"http://45.77.217.95/mobileserver"',
    // baseURL: '"http://localhost:8879"',
    facebookApiID: '1918642011734785',
    siteName: 'TapOnIt',
    shortCode: '82928',
    optinKeyword: 'TapOnIt',
    clientHome: 'http://www.taponitdeals.com',
    phone: '1-844-558-6766',
    copyright: "'Copyright © 2019, TapOnIt'",
    tutorialText: require('../src/assets/themes/taponit/data/tutorial.json'),
    email: 'taponit@taponitdeals.com'
    //helpContent: require('../src/assets/themes/wheredeals/data/help.json'),
  },
  port: 3001
});
