let prodConfig = {
  env: {
    NODE_ENV: '"wallaces"',
    baseURL: '"http://mobileserver-wallaces.mzdymwnxwe.us-east-1.elasticbeanstalk.com"',
    facebookApiID: '1286944314739725',
    siteName: 'Wallace\'s Garden Center',
    shortCode: '35945',
    optinKeyword: 'Wallaces',
    clientHome: 'http://www.wallacesgardencenter.com/',
    phone: '1-844-558-6766',
    copyright: "'Powered by TOItech™'",
    email: 'taponit@taponitdeals.com'
  }
}

module.exports = prodConfig;
