let prodConfig = {
  env: {
    NODE_ENV: '"caseys-dev"',
    baseURL: '"http://mobileserver-caseys-dev.mzdymwnxwe.us-east-1.elasticbeanstalk.com"',
    facebookApiID: '1791281301174721',
    siteName: 'Casey\'s General Store',
    clientHome: 'http://www.caseys.com/',
    copyright: "'Powered by TOItech™'",
    menuContent: require('../src/assets/themes/caseys/data/menu.json'),
    homeIconLink: 'http://caseys.com',
    homeIconLinkNewTab: true
  }
}

module.exports = prodConfig;
