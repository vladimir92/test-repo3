let prodConfig = {
  env: {
    NODE_ENV: '"wheredeals"',
    baseURL: '"http://mobileserver-wheredeals.us-east-1.elasticbeanstalk.com/"',
    facebookApiID: '1918642011734785',
    siteName: 'Where Deals',
    shortCode: '35945',
    optinKeyword: 'VEGASDEALS',
    clientHome: 'http://www.wdeals.net/',
    phone: "'1-800-680-4035'",
    copyright: "'Powered by TOItech™'",
    email: "'wheredeals@wdeals.net'",
    menuContent: require('../src/assets/themes/wheredeals/data/menu.json'),
    offerTypeDescLcase: 'deal',
    offerTypeDescUcase: 'DEAL',
    offerTypeDescPcase: 'Deal',
  }
}

module.exports = prodConfig;
