var merge = require('webpack-merge')
var devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  env: {
    NODE_ENV: '"testing"'
  }
})
